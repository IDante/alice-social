#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 <EntityName>"
  exit 1
fi

ENTITY_NAME=$1
SERVICE_FILE="Service.java"

echo "Generating $SERVICE_FILE for entity $ENTITY_NAME..."

cat > $SERVICE_FILE <<EOL
package com.rentinhand.rihtracker.services;

import com.rentinhand.rihtracker.dto.requests.${ENTITY_NAME}.${ENTITY_NAME}CreateRequest;
import com.rentinhand.rihtracker.dto.requests.${ENTITY_NAME}.${ENTITY_NAME}UpdateRequest;
import com.rentinhand.rihtracker.entities.${ENTITY_NAME};

import java.util.Optional;

public interface ${ENTITY_NAME}Service {
    Optional<${ENTITY_NAME}> findById(Long ${ENTITY_NAME}Id);
    ${ENTITY_NAME} create${ENTITY_NAME}(${ENTITY_NAME}CreateRequest ${ENTITY_NAME}Data);
    ${ENTITY_NAME} update${ENTITY_NAME}(${ENTITY_NAME} ${ENTITY_NAME}, ${ENTITY_NAME}UpdateRequest ${ENTITY_NAME}Data);
    boolean delete${ENTITY_NAME}(${ENTITY_NAME} ${ENTITY_NAME});
}
EOL

echo "File $SERVICE_FILE generated successfully!"

