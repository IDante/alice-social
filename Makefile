#!make
include .env

.PHONY: build
build:
	docker compose build

up:
	docker compose up -d

down:
	docker compose down


deploy:
	docker compose down
	docker compose build --no-cache --parallel
	docker compose up -d --force-recreate