plugins {
    java
    id("com.autonomousapps.dependency-analysis") version "1.21.0"
    id("org.springframework.boot") version "3.1.2"
    id("io.spring.dependency-management") version "1.1.2"
}

group = "com.example"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()

}

dependencies {
    // Spring Docs
    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.2.0")


    // Storage
    implementation("com.amazonaws:aws-java-sdk-core:1.12.458")
    implementation("com.amazonaws:aws-java-sdk-s3:1.12.458")


    // JWT
    implementation("io.jsonwebtoken:jjwt-api:0.11.5")
    implementation("org.springframework.boot:spring-boot-starter-actuator:3.1.2")
    runtimeOnly("io.jsonwebtoken:jjwt-impl:0.11.5")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:0.11.5")
    implementation("io.jsonwebtoken:jjwt:0.9.1")
    implementation("org.webjars.npm:jsonwebtoken:8.5.1")

    // Yandex Passport Auth
    implementation("org.springframework.social:spring-social-core:1.1.6.RELEASE")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.15.2")


    // QRCode
    implementation("com.github.aytchell:qrgen:2.0.0")

    // Pusher
    implementation("com.pusher:pusher-http-java:1.0.0")
    implementation("jakarta.validation:jakarta.validation-api:3.0.2")
    implementation("org.springframework.boot:spring-boot-starter-data-jdbc:3.1.2")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:3.1.2")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-client:3.1.2")
    implementation("org.springframework.boot:spring-boot-starter-security:3.1.2")
    implementation("org.springframework.boot:spring-boot-starter-web:3.1.2")
    runtimeOnly("org.hibernate.validator:hibernate-validator:8.0.1.Final")
    compileOnly("org.projectlombok:lombok")
    runtimeOnly("org.postgresql:postgresql")
    annotationProcessor("org.projectlombok:lombok")
    testImplementation("org.springframework.boot:spring-boot-starter-test:3.1.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.10.0")
    implementation("org.modelmapper:modelmapper:3.1.1")
    implementation("commons-validator:commons-validator:1.7")
    implementation("org.apache.httpcomponents.client5:httpclient5:5.2.1")

    implementation("javax.xml.bind:jaxb-api:2.3.1")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.3")
    implementation("org.springframework:spring-core:6.0.11")
    testImplementation("org.mockito:mockito-core:5.3.1")
    implementation("jakarta.annotation:jakarta.annotation-api:2.1.1")
    implementation("org.springframework.boot:spring-boot-autoconfigure:3.1.2")
    testImplementation("org.assertj:assertj-core:3.24.2")
    implementation("org.springframework.security:spring-security-core:6.1.2")
    implementation("org.springframework.security:spring-security-config:6.1.2")
    implementation("org.apache.httpcomponents:httpcore:4.4.16")
    implementation("org.springframework:spring-web:6.0.11")
    testImplementation("org.springframework.boot:spring-boot-test-autoconfigure:3.1.2")
    implementation("org.springframework:spring-tx:6.0.11")
    implementation("jakarta.persistence:jakarta.persistence-api:3.1.0")
    implementation("org.springframework.security:spring-security-web:6.1.2")
    testImplementation("org.springframework.boot:spring-boot-test:3.1.2")
    testImplementation("org.mockito:mockito-junit-jupiter:5.3.1")
    implementation("org.springframework.security:spring-security-crypto:6.1.2")
    implementation("com.google.code.gson:gson:2.10.1")
    implementation("org.apache.tomcat.embed:tomcat-embed-core:10.1.11")
    implementation("org.hibernate.orm:hibernate-core:6.2.6.Final")
    testImplementation("org.springframework:spring-test:6.0.11")
    implementation("org.springframework.data:spring-data-jpa:3.1.2")
    implementation("org.springframework.boot:spring-boot:3.1.2")
    implementation("org.springframework:spring-context:6.0.11")
    implementation("org.springframework:spring-beans:6.0.11")
    implementation("org.springframework:spring-webmvc:6.0.11")
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.15.2")

    testImplementation("org.springframework.security:spring-security-test:5.7.4")
}

tasks.withType<Test> {
    useJUnitPlatform()
}