package com.example.alice.controllers;

import com.example.alice.models.FileResponse;
import com.example.alice.services.files.FileService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Tag(name = "Работа с файлами", description = "Набор ручек с соответствующими методами, заточенными под" +
        "хранение и скачивание аватарок пользователей, а также голосовых сообщений в/из Yandex Object Storage")
@RestController
@RequestMapping("files")
@AllArgsConstructor
@UserAuth
public class FileController {
    private final FileService service;

    @Operation(summary = "Получить все сохраненные файлы с их метаинформацией")
    @GetMapping
    public ResponseEntity<List<FileResponse>> getFiles() {
        return new ResponseEntity<>(service.getAllFiles(), HttpStatus.OK);
    }

    @Operation(summary = "Загрузить один или несколько файлов с указанием метаданных внутри сервиса")
    @PostMapping(
            path = "",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FileResponse> saveFile(@RequestParam("title") String title,
                                                 @RequestParam("description") String description,
                                                 @RequestParam("file") MultipartFile file) {
        return new ResponseEntity<>(service.saveFile(title, description, file), HttpStatus.OK);
    }

    @Operation(summary = "Скачать изображение по идентификатору")
    @GetMapping(value = "{id}/image/download")
    public byte[] downloadFileImage(@PathVariable("id") Long id) {
        return service.downloadImage(id);
    }

    @Operation(summary = "Скачать изображение по его названию в базе сервиса")
    @GetMapping(value = "{title}/image/download")
    public byte[] downloadFileByTitle(@PathVariable("title") String title) {
        return service.downloadImage(title);
    }
}
