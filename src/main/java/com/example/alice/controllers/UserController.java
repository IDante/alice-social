package com.example.alice.controllers;

import com.example.alice.DTO.contact.ContactDTO;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.models.contacts.Contact;
import com.example.alice.models.users.User;
import com.example.alice.requests.contact.ContactCreateRequest;
import com.example.alice.responses.ContactsResponse;
import com.example.alice.services.UserService;
import com.example.alice.services.contacts.ContactService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import com.example.alice.utilities.SecurityWorkspace;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Tag(name = "Пользователи")
@RestController
@Validated
@AllArgsConstructor
@RequestMapping("users")
@UserAuth
public class UserController {
    private final UserService userService;
    private final ContactService contactService;

    @Operation(summary = "Получить пользователя по идентификатору")
    @GetMapping("/{userId}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("userId") Long userId) {
        return ResponseEntity.ok(new UserDTO(userService.findById(userId)));
    }

    @Operation(summary = "Получить контакты пользователя")
    @GetMapping("/{userId}/contacts")
    public ResponseEntity<ContactsResponse> getUsersContactList(@PathVariable("userId") Long userId) {
        List<ContactDTO> allContactsResponse = contactService.findUsersContactList(userId);
        return ResponseEntity.ok(new ContactsResponse(allContactsResponse));
    }

    @Operation(summary = "Поменять текущую аватарку пользователя")
    @PostMapping(
            path = "/avatar",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> setAvatar(@RequestParam("file") MultipartFile file) {
        return new ResponseEntity<>(userService.saveLogo(SecurityWorkspace.getAuthUserId(), file), HttpStatus.OK);
    }

    @Operation(summary = "Создать контакт у пользователя")
    @PostMapping("/{userId}/create-contact")
    public ResponseEntity<ContactDTO> createContact(
            @RequestBody
            ContactCreateRequest contactCreateRequest,
            @PathVariable("userId") Long userId
    ) {
        Contact contact = contactService.create(contactCreateRequest, userId);
        User user = userService.findById(userId);

        contactService.addContactToUser(contact, user);

        ContactDTO contactDTO = new ContactDTO(contact);

        return ResponseEntity.ok(contactDTO);
    }
}
