package com.example.alice.controllers;

import com.example.alice.DTO.contact.ContactDTO;
import com.example.alice.services.contacts.ContactService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Tag(name = "Контакты")
@RestController
@AllArgsConstructor
@RequestMapping("contacts")
@UserAuth
@CrossOrigin(origins = "*")
public class ContactController {

    private ContactService contactService;

    @Operation(summary = "Получить контакт по идентификатору")
    @GetMapping("/{contactId}")
    public ResponseEntity<ContactDTO> getContact(@PathVariable("contactId") Long contactId) {
        ModelMapper mapper = new ModelMapper();
        ContactDTO contactDTO = mapper.map(contactService.findById(contactId), ContactDTO.class);
        return ResponseEntity.ok(contactDTO);
    }

    @Operation(summary = "Обновить контакт по идентификатору")
    @PutMapping("/update/{contactId}")
    public ResponseEntity<ContactDTO> updateContact(
            @RequestBody
            @Valid
            ContactDTO request,
            @PathVariable("contactId") Long contactId
    ) {
        ModelMapper mapper = new ModelMapper();
        ContactDTO contactDTO = mapper.map(contactService.update(contactId, request), ContactDTO.class);
        return ResponseEntity.ok(contactDTO);
    }
}
