package com.example.alice.controllers;

import com.example.alice.responses.StatusResponse;
import com.example.alice.responses.UsersResponse;
import com.example.alice.services.FriendService;
import com.example.alice.services.QrService;
import com.example.alice.services.UserService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Система друзей | Близкий круг")
@RestController
@Validated
@AllArgsConstructor
@RequestMapping("friends")
@UserAuth
@CrossOrigin(origins = "*")
public class FriendController {
    private final UserService userService;
    private final FriendService friendService;
    private final QrService qrService;

    @Operation(summary = "Получить друзей указанного пользователя")
    @GetMapping("/{userId}")
    public ResponseEntity<UsersResponse> findUserFriends(@PathVariable Long userId) {
        return ResponseEntity.ok(userService.getUserFriends(userId));
    }

    @Operation(summary = "Добавить в друзья")
    @PostMapping("/{userId}/add/{friendUserId}")
    public ResponseEntity<StatusResponse> addFriend(@Parameter(name = "кто хочет добавить в друзья") @PathVariable Long userId,
                                                    @Parameter(name = "друг, которого хочется добавить")
                                                    @PathVariable Long friendUserId) {
        friendService.addFriend(userId, friendUserId);
        return ResponseEntity.ok(new StatusResponse("Friend was added"));
    }

    @Operation(summary = "Убрать человека из друзей")
    @DeleteMapping("/{userId}/delete/{friendUserId}")
    public ResponseEntity<StatusResponse> deleteFriend(@PathVariable Long userId, @PathVariable Long friendUserId) {
        friendService.deleteFriend(userId, friendUserId);
        return ResponseEntity.ok(new StatusResponse("Friend was deleted"));
    }

    @Operation(summary = "Добавить в друзья по qr-коду")
    @GetMapping("/{userId}/add-qr/{friendUserId}")
    public ResponseEntity<byte[]> addFriendWithQR(@PathVariable Long userId, @PathVariable Long friendUserId) {
        String string = "https://alisasharedemo.ru/api/friends/" + userId + "/add/" + friendUserId;
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG)
                .body(qrService.generateQRWithLogo(string));
    }
}
