package com.example.alice.controllers;

import com.example.alice.requests.auth.AuthEmailRequest;
import com.example.alice.requests.register.RegistrationRequest;
import com.example.alice.responses.AuthResponse;
import com.example.alice.services.auth.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Tag(name = "Авторизация")
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class AuthController {
    private final AuthService service;

    @Operation(summary = "Базовая регистрация")
    @PostMapping("/register")
    public ResponseEntity<AuthResponse> register(
            @RequestBody
            @Parameter(name = "Запрос с данными нового пользователя")
            RegistrationRequest data
    ) {
        return ResponseEntity.ok(service.register(data));
    }

    @Operation(summary = "Аутентификация по Яндекс ID",
            description = "Позволяет как зарегистрировать " +
                    "нового пользователя на основе данных из Яндекс ID, так и авторизировать существующего")
    @PostMapping("/login/yandex")
    public ResponseEntity<AuthResponse> authYandex(@Parameter(name = "Токен, полученный из Яндекс Oauth") String access_token
    ) {
        return ResponseEntity.ok(service.authYandex(access_token));
    }

    @Operation(summary = "Базовая аутентификация", description = "Авторизирует существующего пользователя по почте и паролю")
    @PostMapping("/login")
    public ResponseEntity<AuthResponse> authenticateEmail(
            @RequestBody AuthEmailRequest data
    ) {
        return ResponseEntity.ok(service.authenticateEmail(data));
    }
}
