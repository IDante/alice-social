package com.example.alice.controllers;

import com.example.alice.DTO.chat.ChatDTO;
import com.example.alice.DTO.message.MessageDTO;
import com.example.alice.requests.chat.FindChatByUsersRequest;
import com.example.alice.requests.message.MessageRequest;
import com.example.alice.responses.ChatsResponse;
import com.example.alice.responses.MessagesResponse;
import com.example.alice.services.ChatService;
import com.example.alice.services.MessageService;
import com.example.alice.services.UserService;
import com.example.alice.services.ailce.AliceReceiveService;
import com.example.alice.services.integrations.SpeechKit.SpeechKitService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import com.example.alice.utilities.SecurityWorkspace;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Tag(name = "Чаты пользователей", description = "Текущие чаты, сообщения в чатах, " +
        "отправление и получение текстовых и голосовых сообщений. Вся магия происходит здесь")
@RestController
@AllArgsConstructor
@RequestMapping("/chats")
@UserAuth
public class ChatController {
    private UserService userService;
    private ChatService chatService;
    private MessageService messageService;
    private AliceReceiveService aliceReceiveService;


    @Operation(summary = "Получить список чатов для данного пользователя")
    @GetMapping("/user/{userId}")
    public ResponseEntity<ChatsResponse> getChatsList(@Parameter(name = "идентификатор пользователя", example = "92") @PathVariable("userId") Long userId) {
        ChatsResponse allChatsResponse = userService.getUserChats(userId);
        return ResponseEntity.ok(allChatsResponse);
    }

    @Operation(summary = "Получить все сообщения в данном чате")
    @GetMapping("/{chatId}/messages")
    public ResponseEntity<MessagesResponse> getAllChatMessage(@PathVariable("chatId") Long chatId) {
        MessagesResponse allMessagesResponse = chatService.getChatMessages(chatId);
        return ResponseEntity.ok(allMessagesResponse);
    }

    @Operation(summary = "Отправить сообщение в данный чат")
    @PostMapping("/{chatId}/send-message")
    public void sendMessage(
            @RequestBody
            @Valid
            MessageRequest request,
            @PathVariable Long chatId
    ) {
        MessageDTO messageDTO = new MessageDTO(
                null, request.getMessage(), null, SecurityWorkspace.getAuthUser()
        );
        messageService.create(messageDTO, chatId);
    }

    @Operation(summary = "Получить определенный чат по идентификатору")
    @GetMapping("/{chatId}")
    public ResponseEntity<ChatDTO> getChat(@PathVariable("chatId") Long chatId) {
        ModelMapper mapper = new ModelMapper();
        ChatDTO chatDTO = mapper.map(chatService.findById(chatId), ChatDTO.class);
        return ResponseEntity.ok(chatDTO);
    }

    @Operation(summary = "Найти чат между двумя пользователями")
    @PostMapping("/for-users")
    public ResponseEntity<ChatDTO> getChat(@RequestBody FindChatByUsersRequest data) {
        ModelMapper mapper = new ModelMapper();
        ChatDTO chatDTO = mapper.map(chatService.findByUsers(data.getFirstId(), data.getSecondId()), ChatDTO.class);
        return ResponseEntity.ok(chatDTO);
    }

    @Operation(summary = "Получить голосовое сообщение по идентификатору")
    @GetMapping("/get-voice-message/{messageId}")
    public ResponseEntity<byte[]> getVoiceMessage(@PathVariable("messageId") Long messageId) throws IOException {
        byte[] message = aliceReceiveService.findMessageById(messageId);

        return ResponseEntity.ok(message);
    }

    /*@PostMapping("/commands")
    public ResponseEntity<String> sendVoiceMessage(@RequestParam MultipartFile voiceFile) throws IOException {
        String message = speechKitService.recognizeText(voiceFile.getBytes());
        String command =
        switch(message){
            case "Алиса отправь голосовое сообщение ":

                break;

        }

        return ResponseEntity.ok(message);
    }*/

}
