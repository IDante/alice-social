package com.example.alice.controllers;

import com.example.alice.exceptions.BaseException;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.exceptions.ValidationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonIOException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import org.hibernate.MappingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

@Tag(name = "Исключения")
@ControllerAdvice
public class ExceptionAdvice {

    @Operation(summary = "На всё, что не найдено, возвращаем 404 NOT_FOUND")
    @ExceptionHandler(value = NotFoundException.class)
    void handle(HttpServletResponse response, NotFoundException exception) throws IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        ObjectMapper mapper = new ObjectMapper();
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(response.getOutputStream()))) {
            bw.write(mapper.writeValueAsString(new BaseException(exception.getMessage())));
        }
    }


    @Operation(summary = "На всё, что содержит невалидные данные, возвращаем 400 BAD_REQUEST")
    @ExceptionHandler(value = {JsonIOException.class, MappingException.class, ValidationException.class})
    void handle(HttpServletResponse response, JsonIOException exception) throws IOException {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        ObjectMapper mapper = new ObjectMapper();
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(response.getOutputStream()))) {
            bw.write(mapper.writeValueAsString(new ValidationException(exception.getMessage())));
        }
    }

}
