package com.example.alice.controllers;



import com.example.alice.responses.ByteFileResponse;
import com.example.alice.services.ailce.AliceSendService;
import com.example.alice.services.ailce.AliceService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Tag(name = "Команды Алисе")
@RestController
@AllArgsConstructor
@RequestMapping("/alice")
@UserAuth
@CrossOrigin(origins = "*")
public class AliceController {
    private AliceService aliceService;
    private AliceSendService aliceSendService;

    @PostMapping("/commands")
    public ResponseEntity<byte[]> sendVoiceMessage(@RequestParam MultipartFile voiceFile) throws IOException {
        ByteFileResponse response =aliceService.aliceCommand(voiceFile);
        if(response.isError()){
            return new ResponseEntity<>(response.getVoiceFile(), HttpStatusCode.valueOf(HttpStatus.SC_ACCEPTED));
        }
         return ResponseEntity.ok(response.getVoiceFile());
    }


}
