package com.example.alice.controllers;

import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Поделиться ссылкой")
@RestController
@Validated
@AllArgsConstructor
@RequestMapping("share")
@UserAuth
public class ShareController {

    @Operation(summary = "Поделиться в Whats'app")
    @GetMapping("/whatsapp")
    public String shareWhatsapp(@RequestParam String url) {
        return "redirect:https://api.whatsapp.com/send?text=" + url;
    }

    @Operation(summary = "Поделиться в Telegram")
    @GetMapping("/telegram")
    public String shareTelegram(@RequestParam String url, @RequestParam String text) {
        return "redirect:https://telegram.me/share/url?url=" + url + "&text=" + text;
    }
}
