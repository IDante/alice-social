package com.example.alice.controllers;

import com.example.alice.DTO.feature.FeatureDTO;
import com.example.alice.models.users.User;
import com.example.alice.responses.FeaturesResponse;
import com.example.alice.services.FeatureService;
import com.example.alice.services.UserService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import com.example.alice.utilities.SecurityWorkspace;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Tag(name = "Работа с фичами")
@RestController
@AllArgsConstructor
@RequestMapping("/features")
@UserAuth
public class FeatureController {
    private FeatureService featureService;
    private UserService userService;

    @Operation(summary = "Получить все фичи")
    @GetMapping("")
    public ResponseEntity<FeaturesResponse> allFeatures() {
        List<FeatureDTO> viewedFeatures = featureService.all();
        return ResponseEntity.ok(
                new FeaturesResponse(viewedFeatures)
        );
    }

    @Operation(summary = "Получить все просмотренные фичи")
    @GetMapping("viewed")
    public ResponseEntity<FeaturesResponse> viewedFeaturesResponse() {
        User user = userService.findById(SecurityWorkspace.getAuthUserId());
        List<FeatureDTO> viewedFeatures = featureService.getViewedFeatures(user);
        return ResponseEntity.ok(
                new FeaturesResponse(viewedFeatures)
        );
    }

    @Operation(summary = "Просмотреть фичу по идентификатору")
    @PostMapping("{featureId}/view")
    public void viewFeature(
            @NonNull @PathVariable Long featureId
    ) {
        featureService.viewFeature(featureService.findById(featureId), userService.findById(SecurityWorkspace.getAuthUserId()));
    }
}
