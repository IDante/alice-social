package com.example.alice.controllers;

import com.example.alice.services.QrService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Генерация QR-кодов")
@RestController
@AllArgsConstructor
@RequestMapping("qr-code")
@UserAuth
public class QrController {
    private final QrService qrService;

    @Operation(summary = "Получить qr-код с указанной ссылкой")
    @GetMapping("{url}")
    public ResponseEntity<byte[]> GenerateQrCode(@PathVariable String url) {
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG)
                .body(qrService.generateQRWithLogo(url));
    }

    @Hidden
    @GetMapping("easter-egg/{url}")
    public ResponseEntity<byte[]> GenerateEasterEgg(@PathVariable String url) {
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG)
                .body(qrService.generateEasterEggQr(url));
    }
}
