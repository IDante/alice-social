package com.example.alice.controllers;

import com.example.alice.responses.StatusResponse;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Hidden
@Controller
public class LogoutController {
    @GetMapping("/logout")
    public ResponseEntity<StatusResponse> logout(HttpServletRequest request) throws Exception {
        request.logout();
        return ResponseEntity.ok(new StatusResponse("Successfully logged out!"));
    }
}
