package com.example.alice.controllers;

import com.example.alice.responses.NotificationsResponse;
import com.example.alice.responses.StatusResponse;
import com.example.alice.services.notification.NotificationService;
import com.example.alice.utilities.AuthorityAnnotations.UserAuth;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Уведомления")
@RestController
@AllArgsConstructor
@RequestMapping("/notifications")
@UserAuth
public class NotificationController {
    private NotificationService notificationService;

    @Operation(summary = "Получить уведомления")
    @GetMapping("")
    public ResponseEntity<NotificationsResponse> getNotifications() {
        NotificationsResponse notificationsResponse = new NotificationsResponse(notificationService.findAllUnread());
        return ResponseEntity.ok(notificationsResponse);
    }

    @Operation(summary = "Прочитать уведомление")
    @PutMapping("/{notificationId}/read")
    public ResponseEntity<StatusResponse> readNotification(@PathVariable @NotNull Long notificationId) {
        notificationService.readNotification(notificationId);
        return ResponseEntity.ok(new StatusResponse("Notification read!"));
    }

    @Operation(summary = "Прочитать все уведомления")
    @PutMapping("/read")
    public ResponseEntity<StatusResponse> readNotifications() {
        notificationService.readNotifications();
        return ResponseEntity.ok(new StatusResponse("Notifications read!"));
    }
}
