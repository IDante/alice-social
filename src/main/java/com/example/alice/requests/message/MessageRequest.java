package com.example.alice.requests.message;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Запрос на отправку сообщения")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageRequest {
    @JsonProperty("message")
    @NotBlank
    private String message;
}
