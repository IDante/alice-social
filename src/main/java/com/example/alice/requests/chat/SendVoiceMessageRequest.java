package com.example.alice.requests.chat;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Запрос на отправку голосового сообщения")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SendVoiceMessageRequest {
    private String message;
}
