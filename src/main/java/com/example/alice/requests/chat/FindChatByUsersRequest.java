package com.example.alice.requests.chat;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Запрос на поиск чата между двумя пользователями")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindChatByUsersRequest {
    @JsonProperty("first_id")
    private Long firstId;
    @JsonProperty("second_id")
    private Long secondId;
}
