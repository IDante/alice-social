package com.example.alice.requests.register;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Запрос на регистрацию пользователя")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationRequest {
    @Schema(description = "Имя", example = "Казимир")
    @JsonProperty("name")
    @NotBlank
    @Size(min = 1, max = 50)
    private String name;
    @Schema(description = "Фамилия", example = "Малевич")
    @JsonProperty("surname")
    @NotBlank
    @Size(min = 1, max = 50)
    private String surname;
    @Schema(description = "Отчество", example = "Северинович")
    @JsonProperty("midname")
    @NotBlank
    @Size(min = 1, max = 50)
    private String midname;
    @Schema(description = "E-mail", example = "avangard@yandex.ru")
    @NotBlank
    @JsonProperty("email")
    @NotBlank
    private String email;
    @Schema(description = "Пароль", example = "Easy_To_Lose_Hard_To_Find")
    @JsonProperty("password")
    @NotBlank
    @Size(min = 8, max = 50)
    private String password;

}
