package com.example.alice.requests.user;


import com.example.alice.DTO.user.CreateUserDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;

import java.util.List;

public class CreateUserRequest {
    @NotEmpty
    List<@Valid CreateUserDTO> orders;
}
