package com.example.alice.requests.contact;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;

@Schema(description = "Запрос на создание контакта")
@AllArgsConstructor
@Data
public class ContactCreateRequest {
    @JsonProperty("phone")
    @NotBlank
    private String phone;

    @JsonProperty("tag")
    @NotBlank
    private String tag;

    @JsonProperty("name")
    @NotBlank
    private String name;

}
