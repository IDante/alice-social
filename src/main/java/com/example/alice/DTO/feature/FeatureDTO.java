package com.example.alice.DTO.feature;

import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FeatureDTO {
    @Nonnull
    private Long id;

    @Nonnull
    @NotEmpty
    private String name;
}
