package com.example.alice.DTO.contact;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UserContactDTO {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("contact_id")
    @NotBlank
    private Long contactId;

    @JsonProperty("user_id")
    @NotBlank
    private Long userId;
    public UserContactDTO(Long contactId, Long userId){
        this.setContactId(contactId);
        this.setUserId(userId);
    }


}
