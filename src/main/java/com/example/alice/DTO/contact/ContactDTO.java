package com.example.alice.DTO.contact;

import com.example.alice.DTO.chat.ChatDTO;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.models.chats.Chat;
import com.example.alice.models.contacts.Contact;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Контакт пользователя")
@AllArgsConstructor
@Data
@NoArgsConstructor
public class ContactDTO {
    @Schema(description = "Идентификатор")
    @JsonProperty("id")
    private Long id;
    @Schema(description = "Телефон")
    @JsonProperty("phone")
    @NotBlank
    private String phone;

    @JsonProperty("tag")
    @Nullable
    private String tag;

    @Schema(description = "Имя контакта")
    @JsonProperty("name")
    @NotBlank
    private String name;

    @Schema(description = "Пользователь контакта")
    @JsonProperty("user")
    @Nullable
    private UserDTO user;

    @Schema(description = "Наличие Умного Дома")
    @JsonProperty("has_smart_home")
    @Nullable
    private Boolean hasSmartHome = false;

    @Schema(description = "Наличие Яндекс Станции")
    @JsonProperty("has_smart_speaker")
    @Nullable
    private Boolean hasSmartSpeaker = false;

    @Schema(description = "Добавлен ли в друзья")
    @JsonProperty("is_friend")
    @Nullable
    private Boolean isFriend = false;

    @Schema(description = "Чат с контактом")
    @JsonProperty("chat")
    private ChatDTO chat;

    public ContactDTO(Contact contact){
        this.id = contact.getId();
        this.tag = contact.getTag();
        this.phone = contact.getPhone();
        this.name = contact.getName();
        this.hasSmartHome = contact.getHasSmartHome();
        this.hasSmartSpeaker = contact.getHasSmartSpeaker();
        this.user = new UserDTO(contact.getUser());
    }

    public ContactDTO(Contact contact, Boolean isFriend){
        this.id = contact.getId();
        this.tag = contact.getTag();
        this.phone = contact.getPhone();
        this.name = contact.getName();
        this.hasSmartHome = contact.getHasSmartHome();
        this.hasSmartSpeaker = contact.getHasSmartSpeaker();
        this.user = new UserDTO(contact.getUser());
        this.isFriend = isFriend;
    }

    public ContactDTO(Contact contact, Boolean isFriend, Chat chat){
        this.id = contact.getId();
        this.tag = contact.getTag();
        this.phone = contact.getPhone();
        this.name = contact.getName();
        this.hasSmartHome = contact.getHasSmartHome();
        this.hasSmartSpeaker = contact.getHasSmartSpeaker();
        this.user = new UserDTO(contact.getUser());
        this.isFriend = isFriend;
        this.chat = chat == null ? null : new ChatDTO(chat);
    }

}
