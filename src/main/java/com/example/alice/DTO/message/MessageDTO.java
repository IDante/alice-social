package com.example.alice.DTO.message;

import com.example.alice.DTO.user.UserDTO;
import com.example.alice.models.chats.Message;
import com.example.alice.models.users.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MessageDTO {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("message_text")
    @NotBlank
    private String messageText;

    @JsonProperty("attachment")
    private String attachment;

    @JsonProperty("isVoice")
    private boolean isVoice;

    @Nonnull
    @JsonIgnore
    private User user;

    @Nullable
    @JsonIgnore
    private long chatId;

    @Nullable
    @JsonIgnore
    private boolean isViewed;


    @JsonProperty("sender")
    private UserDTO userDTO;

    public MessageDTO(Message message) {
        this.id = message.getId();
        this.messageText = message.getMessageText();
        this.attachment = message.getAttachment();
        this.user = message.getUser();
        this.userDTO = new UserDTO(message.getUser());
        this.isViewed = message.getViewed();
    }

    public MessageDTO(Long id, String messageText, String attachment, @Nonnull User user) {
        this.id = id;
        this.messageText = messageText;
        this.attachment = attachment;
        this.user = user;
    }
}
