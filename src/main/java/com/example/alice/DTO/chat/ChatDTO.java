package com.example.alice.DTO.chat;

import com.example.alice.DTO.message.MessageDTO;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.models.chats.Chat;
import com.example.alice.models.chats.Message;
import com.example.alice.models.users.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@Schema(description = "Чат пользователя")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ChatDTO {
    @Schema(description = "Идентификатор")
    @JsonProperty("id")
    private Long id;
    @Schema(description = "Название чата")
    @JsonProperty("title")
    @NotBlank
    @Size(min = 1, max = 255)
    private String title;

    @Schema(description = "Пользователи в данном чате")
    @JsonProperty("users")
    private List<UserDTO> users;

    @Schema(description = "Последнее сообщение в чате")
    @JsonProperty("last_message")
    private MessageDTO lastMessage;

    public ChatDTO(Chat chat) {
        this.id = chat.getId();
        this.title = chat.getTitle();
        this.users = chat.getUsers().stream().map(UserDTO::new).toList();
        lastMessage = new MessageDTO(chat.getLastMessage());
    }
}
