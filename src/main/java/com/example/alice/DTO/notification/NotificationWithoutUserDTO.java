package com.example.alice.DTO.notification;

import com.example.alice.DTO.user.UserDTO;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.NotificationType;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationWithoutUserDTO {
    private Long id;
    private NotificationType notificationType;
    private String notificationText;
    private JsonNode body;
    private boolean viewed;


    public boolean isMessage(){
        return this.getNotificationType().equals(NotificationType.MESSAGE);
    }
}
