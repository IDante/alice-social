package com.example.alice.DTO.notification;

import com.example.alice.DTO.user.UserDTO;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.NotificationType;
import com.example.alice.models.users.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDTO {
    private User user;
    private NotificationType notificationType;
    private String notificationText;
    private String body;
    private boolean viewed;
}
