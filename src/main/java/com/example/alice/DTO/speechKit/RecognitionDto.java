package com.example.alice.DTO.speechKit;

import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.LanguageEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class RecognitionDto {
    @JsonProperty("lang")
    private String lang = LanguageEnum.RU.getLanguageName();

    @JsonProperty("topic")
    private String topic = "general";

    @JsonProperty("profanityFilter")
    private boolean profanityFilter = false;

}
