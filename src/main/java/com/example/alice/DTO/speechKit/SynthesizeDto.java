package com.example.alice.DTO.speechKit;

import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.EmotionEnum;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.VoiceEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

@AllArgsConstructor
@Getter
@Setter
public class SynthesizeDto {
    @JsonProperty("text")
    private String text;

    @JsonProperty("voice")
    private VoiceEnum voice = VoiceEnum.ALENA;

    @JsonProperty("emotion")
    private EmotionEnum emotion = EmotionEnum.NEUTRAL;

    @JsonProperty("speed")
    private Double speed = 1.0;

    public SynthesizeDto(String text) {
        this.text = text;
    }

    public void setSpeed(Double speed) {
        if(speed > 3.0){
            this.speed = 3.0;
        }else if(speed < 0.1){
            this.speed = 0.1;
        }
        else {
            this.speed = speed;
        }
    }

    public void setEmotion(EmotionEnum emotion){
        if(this.voice.supportsEmotion(emotion)){
            this.emotion = emotion;
        }
    }
}
