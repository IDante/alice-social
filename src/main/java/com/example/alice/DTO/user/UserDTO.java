package com.example.alice.DTO.user;

import com.example.alice.models.users.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.validation.annotation.Validated;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Validated
public class UserDTO {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    @Nonnull
    @Size(min = 1, max = 50)
    private String name;

    @JsonProperty("surname")
    @NotBlank
    @Size(min = 1, max = 50)
    private String surname;

    @JsonProperty("midname")
    private String midname;

    @JsonProperty("email")
    @NotBlank
    @Size(min = 1, max = 50)
    private String email;

    @JsonProperty("avatar_url")
    private String avatarUrl;

    public UserDTO(User user) {
        id = user.getId();
        name = user.getName();
        surname = user.getSurname();
        midname = user.getMidname();
        email = user.getEmail();
        avatarUrl = user.getAvatarUrl();
    }
}
