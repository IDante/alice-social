package com.example.alice.DTO.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class CreateUserDTO{
    @JsonProperty("id")
    Long id;
    @JsonProperty("name")
    @NotBlank
    @Size(min = 1, max = 50)
    String name;
    @JsonProperty("surname")
    @NotBlank
    @Size(min = 1, max = 50)
    String surname;
    @JsonProperty("midname")
    @NotBlank
    @Size(min = 1, max = 50)
    String midname;
    @JsonProperty("email")
    @NotBlank
    @Size(min = 1, max = 50)
    String email;
}
