package com.example.alice.exceptions;

public class PusherException extends BaseException{
    public PusherException(String message) {
        super(message);
    }
}
