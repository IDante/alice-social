package com.example.alice.exceptions;

public class AliceNotFoundException extends BaseException{
    public AliceNotFoundException(String message) {
        super(message);
    }
}
