package com.example.alice.exceptions;

public class BaseException extends RuntimeException{
    public BaseException(String message) {
        super(message);
    }
}
