package com.example.alice.exceptions;

public class NotFoundException extends BaseException{
    public NotFoundException(String message) {
        super(message);
    }
}
