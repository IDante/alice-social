package com.example.alice.exceptions;

public class QrCodeException extends  BaseException{
    public QrCodeException(String message) {
        super(message);
    }
}
