package com.example.alice.repos;

import com.example.alice.models.FileResponse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<FileResponse, Long> {
    FileResponse findByTitle(String title);
}
