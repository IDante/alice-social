package com.example.alice.repos;

import com.example.alice.models.users.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);

    List<User> findByName(String name);



    Set<User> findByIdOrFriends_Id(Long id, Long friendId);
}