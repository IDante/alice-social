package com.example.alice.repos;

import com.example.alice.models.AliceConstans;
import com.example.alice.models.Feature;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AliceConstantsRepository extends JpaRepository<AliceConstans, Long> {
    List<AliceConstans> findByConstant(String constant);

}