package com.example.alice.repos;

import com.example.alice.models.chats.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findByChat_IdAndUser_IdNotAndIsVoiceTrueOrderByIdDesc(Long id, Long id1);

    List<Message> findByIsVoiceTrueAndChat_Users_IdAndViewedFalse(Long id);





}