package com.example.alice.repos;

import com.example.alice.DTO.message.MessageDTO;
import com.example.alice.models.chats.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ChatRepository extends JpaRepository<Chat, Long> {
    Optional<Chat> findByTitle(String title);
}