package com.example.alice.repos.contacts;

import com.example.alice.models.contacts.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Long> {
    List<Contact> findByName(String name);
    List<Contact> findByUser_Id(Long id);
}