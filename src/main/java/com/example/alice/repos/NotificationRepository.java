package com.example.alice.repos;

import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.NotificationType;
import com.example.alice.models.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
    List<Notification> findByUser_IdAndViewed(Long id, Boolean viewed);

    Notification findByIdAndUser_Name(Long id, String name);

    List<Notification> findByNotificationTypeAndUser_IdAndViewedFalse(NotificationType notificationType, Long id);




}