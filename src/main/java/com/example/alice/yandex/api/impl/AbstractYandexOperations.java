package com.example.alice.yandex.api.impl;

import org.springframework.social.MissingAuthorizationException;

public class AbstractYandexOperations {

    private final boolean isAuthorized;

    public AbstractYandexOperations(boolean isAuthorized) {
        this.isAuthorized = isAuthorized;
    }

    protected void requireAuthorization() {
        if (!isAuthorized) {
            throw new MissingAuthorizationException("yandex");
        }
    }
}