package com.example.alice.yandex.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class YandexPassport {

    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("display_name")
    private String displayName;
    private List<String> emails;
    @JsonProperty("default_email")
    private String defaultEmail;
    @JsonProperty("real_name")
    private String realName;
    private String birthday;
    private String login;
    @JsonProperty("old_social_login")
    private String oldSocialLogin;
    private String sex;
    private Long id;
    @JsonProperty("default_avatar_id")
    private String defaultAvatarId;

}
