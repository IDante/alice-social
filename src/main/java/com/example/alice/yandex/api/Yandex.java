package com.example.alice.yandex.api;

import org.springframework.social.ApiBinding;

public interface Yandex extends ApiBinding {

    UserOperations userOperations();

}