package com.example.alice.yandex.api.impl;

import com.example.alice.yandex.api.UserOperations;
import com.example.alice.yandex.api.Yandex;
import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;

public class YandexTemplate extends AbstractOAuth2ApiBinding implements Yandex {

    private UserOperations userOperations;

    public YandexTemplate(String accessToken) {
        super(accessToken);

        initSubApis();
    }

    private void initSubApis() {
        userOperations = new UserTemplate(getRestTemplate(), isAuthorized());
    }

    @Override
    public UserOperations userOperations() {
        return userOperations;
    }
}
