package com.example.alice.yandex.api.impl;

import com.example.alice.yandex.api.UserOperations;
import com.example.alice.yandex.api.YandexPassport;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public class UserTemplate extends AbstractYandexOperations implements UserOperations {

    private final RestTemplate restTemplate;

    public UserTemplate(RestTemplate restTemplate, boolean isAuthorizedForUser) {
        super(isAuthorizedForUser);
        this.restTemplate = restTemplate;
    }

    @Override
    public YandexPassport getPassport() {
        return getRestTemplate().getForObject(buildUri(), YandexPassport.class);
    }

    private URI buildUri() {
        return UriComponentsBuilder.fromHttpUrl("https://login.yandex.ru/info?format=json")
                .build()
                .toUri();
    }

    private RestTemplate getRestTemplate() {
        return restTemplate;
    }
}
