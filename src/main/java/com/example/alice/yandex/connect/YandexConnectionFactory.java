package com.example.alice.yandex.connect;


import com.example.alice.yandex.api.Yandex;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;

public class YandexConnectionFactory extends OAuth2ConnectionFactory<Yandex> {

    public YandexConnectionFactory(String clientId, String clientSecret) {
        super("yandex", new YandexServiceProvider(clientId, clientSecret), new YandexAdapter());
    }
}
