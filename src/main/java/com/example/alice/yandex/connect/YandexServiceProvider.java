package com.example.alice.yandex.connect;

import com.example.alice.yandex.api.Yandex;
import com.example.alice.yandex.api.impl.YandexTemplate;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Template;

public class YandexServiceProvider extends AbstractOAuth2ServiceProvider<Yandex> {

    public YandexServiceProvider(String clientId, String clientSecret) {
        super(new OAuth2Template(clientId, clientSecret,
                "https://oauth.yandex.ru/authorize",
                "https://oauth.yandex.ru/token"));
    }

    public Yandex getApi(String accessToken) {
        return new YandexTemplate(accessToken);
    }

}
