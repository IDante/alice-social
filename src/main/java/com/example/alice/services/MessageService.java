package com.example.alice.services;

import com.example.alice.DTO.message.MessageDTO;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.models.chats.Chat;
import com.example.alice.models.chats.Message;
import com.example.alice.publishers.message.MessageEventPublisher;
import com.example.alice.repos.MessageRepository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;
    private final MessageEventPublisher eventPublisher;
    private final ChatService chatService;

    @Transactional
    public Message create(MessageDTO messageDTO, Long chatId) {
        Chat chat = chatService.findById(chatId);
        Message message = new Message();
        message.setChat(chat);
        message.setMessageText(messageDTO.getMessageText());
        message.setAttachment(messageDTO.getAttachment());
        message.setUser(messageDTO.getUser());
        message.setVoice(messageDTO.isVoice());
        message.setViewed(false);
        message = messageRepository.save(message);

        eventPublisher.publishCreated(message);

        return message;
    }

    @Transactional
    public Message update(Message message, MessageDTO messageDTO, Long chatId) {
        Chat chat = chatService.findById(chatId);
        message.setChat(chat);
        message.setMessageText(messageDTO.getMessageText());
        message.setAttachment(messageDTO.getAttachment());
        message.setViewed(messageDTO.isViewed());
        return messageRepository.save(message);
    }

    public Message findById(Long id) {
        return messageRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No such message"));
    }

    @Transactional
    public void deleteById(Long id) {
        try {
            messageRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new NotFoundException("No such user");
        }
    }

    private void updateModel(Message newMessage, Message message) {
        if (!message.getMessageText().equals(newMessage.getMessageText())) {
            message.setMessageText(newMessage.getMessageText());
        }
        if (!message.getAttachment().equals(newMessage.getAttachment())) {
            message.setAttachment(newMessage.getAttachment());
        }
    }

    public Message findMessageById(Long id) {
        Optional<Message> message = messageRepository.findById(id);
        if (message.isPresent())
            return message.get();
        throw new NotFoundException("No such message");
    }

    @Transactional
    public void updateOrSaveMessage(Message newMessage) {
        Message message = messageRepository.findById(newMessage.getId()).orElse(null);
        if (message != null) {
            updateModel(newMessage, message);
        } else {
            messageRepository.save(newMessage);
        }
    }

    @Transactional
    public Message readMessage(Message message){
        message.setViewed(true);
        return messageRepository.save(message);
    }


    @Transactional
    public void deleteMessageById(Long id) {
        try {
            messageRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new NotFoundException("No such message");
        }
    }
}
