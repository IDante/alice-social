package com.example.alice.services;

import com.example.alice.DTO.chat.ChatDTO;
import com.example.alice.models.chats.Message;
import com.example.alice.repos.MessageRepository;
import com.example.alice.responses.ChatsResponse;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.responses.UsersResponse;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.models.FileResponse;
import com.example.alice.models.Notification;
import com.example.alice.models.users.User;
import com.example.alice.repos.UserRepository;
import com.example.alice.services.files.FileService;
import com.example.alice.validators.UserDTOValidator;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.DataBinder;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class UserService {
    private final UserDTOValidator userDTOValidator;
    private final UserRepository userRepository;
    private final FileService fileService;
    private final MessageRepository messageRepository;

    @Transactional
    public User create(UserDTO userDTO) {

        final DataBinder dataBinder = new DataBinder(userDTO);
        dataBinder.addValidators(userDTOValidator);
        dataBinder.validate();
        ModelMapper modelMapper = new ModelMapper();
        User user = modelMapper.map(userDTO, User.class);
        return userRepository.save(user);
    }

    @Transactional
    public User update(User user, UserDTO newUserDTO) {
        final DataBinder dataBinder = new DataBinder(newUserDTO);
        dataBinder.addValidators(userDTOValidator);
        dataBinder.validate();
        user.setName(newUserDTO.getName());
        user.setSurname(newUserDTO.getSurname());
        user.setMidname(newUserDTO.getMidname());
        user.setEmail(newUserDTO.getEmail());
        return userRepository.save(user);
    }

    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No such user"));
    }

    public List<User> findUsersByName(String name) {
        return userRepository.findByName(name);
    }
    @Transactional
    public void deleteById(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new NotFoundException("No such user");
        }
    }

    public ChatsResponse getUserChats(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("No such user"));
        ModelMapper mapper = new ModelMapper();
        List<ChatDTO> chatDTOList = user
                .getChats()
                .stream()
                .map((element) -> mapper.map(element, ChatDTO.class))
                .toList();
        return new ChatsResponse(chatDTOList);
    }

    public UsersResponse getUserFriends(Long userId) {
        User userModel = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("No such user"));
        ModelMapper mapper = new ModelMapper();
        return new UsersResponse(userModel
                .getFriends()
                .stream()
                .map((element) -> mapper.map(element, UserDTO.class))
                .toList());
    }

    public String saveLogo(Long user_id, MultipartFile file) {
        User user = userRepository.findById(user_id).get();
        FileResponse fileResponse = fileService.saveFile("avatar"+ user_id, String.valueOf(user_id), file);
        String avatarUrl = fileResponse.getFullUrl();
        user.setAvatarUrl(avatarUrl);
        userRepository.save(user);
        return avatarUrl;
    }

    public List<Notification> getUserNotifications(Long userId) {
        return findById(userId).getNotifications();
    }
}