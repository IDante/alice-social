package com.example.alice.services.integrations;

import com.example.alice.exceptions.PusherException;
import com.pusher.rest.Pusher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;

@Service
public class PusherService {
    private Pusher pusher;

    @Value("${pusher.id}")
    private String appId;
    @Value("${pusher.key}")
    private String key;
    @Value("${pusher.secret}")
    private String secret;

    public PusherService() {
        pusher = new Pusher("1650530", "bdc421ed9c62305ae6fb", "9932b0a839bf0ede4974");
        pusher.setCluster("mt1");
        pusher.setEncrypted(true);
    }

    @Async
    public void send(String channel, String eventName, Map<String, String> data){
        var result = pusher.trigger(channel, eventName, data);
        if (result.getHttpStatus() != 200){
            throw new PusherException("Message Not Sent!");
        }
    }
}
