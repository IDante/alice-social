package com.example.alice.services.integrations.SpeechKit;

import com.example.alice.DTO.speechKit.RecognitionDto;
import com.example.alice.DTO.speechKit.SynthesizeDto;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.EmotionEnum;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.VoiceEnum;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.models.chats.Message;
import com.example.alice.repos.MessageRepository;
import com.nimbusds.jose.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Optional;


@Service
@Setter
@Transactional(readOnly = true)
@AllArgsConstructor
public class SpeechKitService {

    private final Pair<String, String> authProps;

    private static String synthesizeMessageUrl = "https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize";

    private static String recognitionMessageUrl = "https://stt.api.cloud.yandex.net/speech/v1/stt:recognize";

    private final MessageRepository messageRepository;

    public byte[] synthesizeText(SynthesizeDto synthesizeDto) throws IOException {
        MultiValueMap<String, String> body = this.buildSynthesizeTextBody(synthesizeDto);
        HttpEntity<MultiValueMap<String, String>> entity = this.buildSynthesizingHttpEntity(body);

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<byte[]> response = restTemplate.postForEntity(synthesizeMessageUrl, entity, byte[].class);


        return response.getBody();
    }

    public String recognizeText(byte[] audio) throws IOException {
        HttpEntity<byte[]> entity = this.buildRecognitionHttpEntity(audio);

        String url = recognitionMessageUrl + "?folderId="  + authProps.getRight();

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

        return  response.getBody();
    }


    public HttpEntity<byte[]> buildRecognitionHttpEntity(byte[] filePath) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set(HttpHeaders.AUTHORIZATION, "Api-Key " + authProps.getLeft());
        return new HttpEntity<>(filePath, headers);
    }

    public HttpEntity<MultiValueMap<String, String>> buildSynthesizingHttpEntity(MultiValueMap<String, String> body) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set(HttpHeaders.AUTHORIZATION, "Api-Key " + authProps.getLeft());
        return new HttpEntity<>(body, headers);
    }

    private MultiValueMap<String, String> buildSynthesizeTextBody(SynthesizeDto synthesizeDto) {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("folderId", authProps.getRight());
        body.add("text", synthesizeDto.getText());
        body.add("voice", synthesizeDto.getVoice().getName());
        body.add("emotion", synthesizeDto.getEmotion().getEmotionName());
        body.add("speed", String.valueOf(synthesizeDto.getSpeed()));
        body.add("lang", synthesizeDto.getVoice().getLanguage().getLanguageName());
        return body;
    }

    public byte[]  getVoiceMessageById(Long id) throws IOException {
        Optional<Message> message = messageRepository.findById(id);
        if (message.isPresent() && message.get().isVoice()) {
            SynthesizeDto synthesizeDto = new SynthesizeDto(message.get().getMessageText());
            synthesizeDto.setVoice(VoiceEnum.JANE);
            synthesizeDto.setEmotion(EmotionEnum.EVIL);
            return this.synthesizeText(synthesizeDto);
        }
        throw new NotFoundException("No such message");
    }


    private MultiValueMap<String, String> buildRecognitionTextBody(RecognitionDto recognitionDto) {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("folderId", authProps.getRight());
        body.add("lang", recognitionDto.getLang());
        body.add("topic", recognitionDto.getTopic());
        body.add("emotion", String.valueOf(recognitionDto.isProfanityFilter()));
        return body;
    }
}
