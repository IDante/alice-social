package com.example.alice.services;

import com.example.alice.DTO.feature.FeatureDTO;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.models.Feature;
import com.example.alice.models.users.User;
import com.example.alice.repos.FeatureRepository;
import com.example.alice.repos.UserRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class FeatureService {
    private final FeatureRepository featureRepository;
    private final UserRepository userRepository;

    @Transactional
    public Feature create(String name) {
        Feature feature = new Feature();
        feature.setName(name);
        return featureRepository.save(feature);
    }

    @Transactional
    public Feature update(Feature feature, String newName) {
        feature.setName(newName);
        return featureRepository.save(feature);
    }

    public Feature findById(Long id) {
        return featureRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No such feature"));
    }

    @Transactional
    public void deleteFeatureById(Long id) {
        try {
            featureRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new NotFoundException("No such feature");
        }
    }

    public List<FeatureDTO> all() {
        ModelMapper mapper = new ModelMapper();
        return featureRepository.findAll()
                .stream()
                .map((Feature feature) -> mapper.map(feature, FeatureDTO.class))
                .toList();
    }

    public List<FeatureDTO> getViewedFeatures(User user) {
        ModelMapper mapper = new ModelMapper();
        return user.getViewedFeatures()
                .stream()
                .map((element) -> mapper.map(element, FeatureDTO.class))
                .toList();
    }

    @Transactional
    public void viewFeature(Feature feature, User user) {
        var features = user.getViewedFeatures();
        feature.getUsers().add(user);
        features.add(feature);
        user.setViewedFeatures(features);

        userRepository.save(user);
    }
}
