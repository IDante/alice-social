package com.example.alice.services;

import com.example.alice.events.friend.FriendRequest;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.exceptions.ValidationException;
import com.example.alice.models.users.User;
import com.example.alice.publishers.friend.FriendEventPublisher;
import com.example.alice.repos.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@AllArgsConstructor
public class FriendService {
    private final UserRepository userRepository;
    private final FriendEventPublisher friendEventPublisher;


    @Transactional
    public void addFriend(Long userId, Long friendUserId) {
        if (Objects.equals(userId, friendUserId)){
            throw new ValidationException("Wrong friend user id");
        }
        User user = userRepository.findById(userId).orElse(null);
        User friendUser = userRepository.findById(friendUserId).orElse(null);
        if(user == null){
            throw new NotFoundException("No such user");
        }
        if(friendUser == null){
            throw new NotFoundException("No such friend user");
        }

        user.getFriends().add(friendUser);

        if (friendUser.getFriends().contains(user)){
            friendEventPublisher.publishFriendRequestAccepted(user, friendUser);
        }
        else{
            friendEventPublisher.publishFriendRequestSent(user, friendUser);
        }
        userRepository.save(user);
    }

    @Transactional
    public void deleteFriend(Long userId, Long friendUserId) {
        User userModel = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("No such user"));
        User friendModel = userRepository.findById(friendUserId).orElseThrow(() -> new NotFoundException("No such friend"));

        userModel.getFriends().remove(friendModel);
        friendModel.getFriends().remove(userModel);

        userRepository.save(userModel);
        userRepository.save(friendModel);
    }
}
