package com.example.alice.services.notification;

import com.example.alice.DTO.notification.NotificationDTO;
import com.example.alice.DTO.notification.NotificationWithoutUserDTO;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.NotificationType;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.models.Notification;
import com.example.alice.models.chats.Message;
import com.example.alice.models.users.User;
import com.example.alice.repos.NotificationRepository;
import com.example.alice.services.UserService;
import com.example.alice.utilities.SecurityWorkspace;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class NotificationService {
    private NotificationRepository notificationRepository;
    private UserService userService;

    public Notification create(NotificationDTO notificationDTO){
        ModelMapper mapper = new ModelMapper();
        Notification notification = mapper.map(notificationDTO, Notification.class);
        notification.setViewed(false);
        return notificationRepository.save(notification);
    }

    public Notification findByIdAndUserName(long id, String userName){
        return this.notificationRepository.findByIdAndUser_Name(id, userName);
    }

    public List<NotificationWithoutUserDTO> findAllUnread(){
        ObjectMapper objectMapper = new ObjectMapper();
        return notificationRepository.findByUser_IdAndViewed(SecurityWorkspace.getAuthUserId(), false)
                .stream().map((element) -> {
                    try {
                        return new NotificationWithoutUserDTO(
                                element.getId(),
                                element.getNotificationType(),
                                element.getNotificationText(),
                                objectMapper.readTree(element.getBody()),
                                element.getViewed()
                        );
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                }).toList();
    }

    public Notification findById(Long notificationId){
        return notificationRepository.findById(notificationId)
                .orElseThrow(() -> new NotFoundException("No such notification"));
    }

    public void readNotification(Long notificationId){
        Notification notification = findById(notificationId);
        notification.setViewed(true);
        notificationRepository.save(notification);
    }

    public void readNotification(Notification notification){
        notification.setViewed(true);
        notificationRepository.save(notification);
    }

    public void readNotifications(){
        var result = userService.findById(SecurityWorkspace.getAuthUserId()).getNotifications()
                .stream().filter((Notification notification) -> notification.getNotificationType() == NotificationType.MESSAGE);
        result.forEach(this::readNotification);
    }

    public void findAndReadNotificationByMessage(Message message){
        ObjectMapper objectMapper = new ObjectMapper();
        User destUser = SecurityWorkspace.getAuthUser();
        List<Notification> notifications = notificationRepository.findByNotificationTypeAndUser_IdAndViewedFalse(NotificationType.MESSAGE, destUser.getId());
        Notification messageNotification = notifications.stream().filter(notification -> {
            try {
                return objectMapper.readTree(notification.getBody()).get("message_id").toString().equals(message.getId().toString());
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).toList().get(0);
        messageNotification.setViewed(true);
        notificationRepository.save(messageNotification);
    }
}
