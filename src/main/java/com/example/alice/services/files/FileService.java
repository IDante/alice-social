package com.example.alice.services.files;

import com.example.alice.models.FileResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {
    FileResponse saveFile(String title, String description, MultipartFile file);

    byte[] downloadImage(Long id);

    byte[] downloadImage(String title);

    List<FileResponse> getAllFiles();
}