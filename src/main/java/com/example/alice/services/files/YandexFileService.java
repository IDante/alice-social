package com.example.alice.services.files;

import com.example.alice.configs.BucketName;
import com.example.alice.models.FileResponse;
import com.example.alice.repos.FileRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

import static org.apache.http.entity.ContentType.*;

@Service
@AllArgsConstructor
public class YandexFileService implements FileService {
    private final FileStore fileStore;
    private final FileRepository repository;

    @Override
    public FileResponse saveFile(String title, String description, MultipartFile file) {
        //check if the file is empty
        if (file.isEmpty()) {
            throw new IllegalStateException("Cannot upload empty file");
        }
        //Check if the file is an image
        if (!Arrays.asList(
                "audio/ogg",
                IMAGE_PNG.getMimeType(),
                IMAGE_SVG.getMimeType(),
                IMAGE_PNG.getMimeType(),
                IMAGE_BMP.getMimeType(),
                IMAGE_GIF.getMimeType(),
                IMAGE_JPEG.getMimeType()).contains(file.getContentType())) {
            throw new IllegalStateException("FIle uploaded is not an image");
        }
        //get file metadata
        Map<String, String> metadata = new HashMap<>();
        metadata.put("Content-Type", file.getContentType());
        metadata.put("Content-Length", String.valueOf(file.getSize()));
        String path = String.format("%s/%s", BucketName.BUCKET_IMAGE.getBucketName(), description);

        String fileName = String.format("%s", file.getOriginalFilename());
        try {
            fileStore.upload(path, fileName, Optional.of(metadata), file.getInputStream());
        } catch (IOException e) {
            throw new IllegalStateException("Failed to upload file", e);
        }
        String fullUrl = "https://storage.yandexcloud.net/"+path+"/"+fileName;
        FileResponse fileResponse = FileResponse.builder()
                .description(description)
                .title(title)
                .imagePath(path)
                .imageFileName(fileName)
                .fullUrl(fullUrl)
                .build();
        return repository.save(fileResponse);
    }

    @Override
    public byte[] downloadImage(Long id) {
        FileResponse fileResponse = repository.findById(id).get();
        return fileStore.download(fileResponse.getImagePath(), fileResponse.getImageFileName());
    }

    @Override
    public byte[] downloadImage(String title) {
        FileResponse fileResponse = repository.findByTitle(title);
        return fileStore.download(fileResponse.getImagePath(), fileResponse.getImageFileName());
    }

    @Override
    public List<FileResponse> getAllFiles() {
        return new ArrayList<>(repository.findAll());
    }
}