package com.example.alice.services.ailce;



import com.example.alice.models.AliceConstans;
import com.example.alice.repos.AliceConstantsRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AliceConstantsService {

    public final AliceConstantsRepository aliceConstantsRepository;


    public String getAskToAnswerToMessageText(){
        return ",  " + aliceConstantsRepository.findByConstant("ASK_TO_ANSWER_TO_MESSAGE").get(0).getText();
    }

    public String getAskToListenMessageText(){
        return aliceConstantsRepository.findByConstant("ASK_TO_LISTEN_MESSAGE").get(0).getText();
    }

    public String getWhoToAskToListenMessageText(){
        return ", " + aliceConstantsRepository.findByConstant("ASK_WHO_TO_LISTEN_MESSAGE").get(0).getText();
    }


    public String getNUnreadedMessagesText(){
        return aliceConstantsRepository.findByConstant("N_UNREADED_MESSAGES").get(0).getText();
    }
    public String getOneUnreadedMessagesText(){
        return aliceConstantsRepository.findByConstant("ONE_UNREADED_MESSAGES").get(0).getText();
    }
    public String getSomeUnreadedMessagesText(){
        return aliceConstantsRepository.findByConstant("SOME_UNREADED_MESSAGES").get(0).getText();
    }

    public String getSuccessMessageText(){
        return aliceConstantsRepository.findByConstant("SUCCESS_MESSAGE").get(0).getText();
    }

    public List<String> getSendVoiceCommands(){
        return aliceConstantsRepository.findByConstant("SEND_VOICE_COMMAND").stream().map(AliceConstans::getText).toList();
    }

    public List<String> getReceiveVoiceCommands(){
        return aliceConstantsRepository.findByConstant("RECEIVE_VOICE_COMMAND").stream().map(AliceConstans::getText).toList();
    }

    public List<String> getVoiceNames(){
        return aliceConstantsRepository.findByConstant("VOICE_NAME").stream().map(AliceConstans::getText).toList();
    }

    public List<String> getTargetUsers(){
        return aliceConstantsRepository.findByConstant("TARGET_USER").stream().map(AliceConstans::getText).toList();
    }

    public List<String> getListForNameSearch(){
        return aliceConstantsRepository.findByConstant("FOR_NAME_SEARCH").stream().map(AliceConstans::getText).toList();
    }

    public List<String> getAvailableNames(){
        return aliceConstantsRepository.findByConstant("AVAILABLE_NAME").stream().map(AliceConstans::getText).toList();
    }

    public String getMom(){
        return aliceConstantsRepository.findByConstant("MOM").get(0).getText();
    }



    public String getAssistantIsListeningMessage(){
        return aliceConstantsRepository.findByConstant("ASSISTANT_IS_LISTENING_MESSAGE").get(0).getText();
    }


    public List<String> getVoiceAssistantNames(){
        return aliceConstantsRepository.findByConstant("VOICE_ASSISTANT").stream().map(AliceConstans::getText).toList();
    }


    public List<String> getReadFromTargetUsers(){
        return aliceConstantsRepository.findByConstant("READ_FROM_TARGET_USER").stream().map(AliceConstans::getText).toList();
    }

    public List<String> getReceiveNewsCommands(){
        return aliceConstantsRepository.findByConstant("RECEIVE_NEWS_COMMAND").stream().map(AliceConstans::getText).toList();
    }

    public String getUserNotFoundMessageText(){
        return aliceConstantsRepository.findByConstant("USER_NOT_FOUND_MESSAGE").get(0).getText();
    }

    public String getNoNotificationsMessageText(){
        return aliceConstantsRepository.findByConstant("NO_NOTIFICATIONS_MESSAGE").get(0).getText();
    }


}
