package com.example.alice.services.ailce;

import com.amazonaws.util.json.Jackson;
import com.example.alice.DTO.speechKit.SynthesizeDto;
import com.example.alice.models.contacts.Contact;
import com.example.alice.models.users.User;
import com.example.alice.repos.contacts.ContactRepository;
import com.example.alice.responses.ByteFileResponse;
import com.example.alice.services.UserService;
import com.example.alice.services.integrations.SpeechKit.SpeechKitService;
import com.example.alice.utilities.SecurityWorkspace;
import jakarta.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
@AllArgsConstructor
public class AliceService {

    private final AliceSendService aliceSendService;
    private final SpeechKitService speechKitService;
    private final HttpSession httpSession;
    private final AliceReceiveService aliceReceiveService;
    private final UserService userService;
    private final AliceConstantsService aliceConstantsService;
    private final ContactRepository contactRepository;


    public ByteFileResponse aliceCommand(MultipartFile voiceFile) throws IOException {
        ByteFileResponse response = null;
        try {
            String message = Jackson.stringMapFromJsonString(speechKitService
                            .recognizeText(voiceFile.getBytes()))
                    .get("result");
            String sessionCommand = this.getLastCommandFromSession();

            switch (sessionCommand) {
                case "ASK_WHO_TO_LISTEN_MESSAGE" -> {
                    if(Objects.equals(message.toLowerCase(), "от мамы")) {
                        message = aliceConstantsService.getMom();
                    }
                    return aliceReceiveService.aliceReadLastMessages(this.findFriend(message));
                }
                case "ASK_TO_LISTEN_MESSAGE" -> {
                    String userName = (String) httpSession.getAttribute(SecurityWorkspace.getAuthUserId() + "-user");
                    return aliceReceiveService.aliceReadLastMessages(this.findFriend(userName));
                }
                case "MESSAGE_PREPARED" -> {
                    return aliceSendService.sendMessage(voiceFile);
                }
            }


            String firstWord = message.split(" ")[0];
            if (aliceConstantsService.getVoiceAssistantNames().contains(firstWord)) {
                String command = extractWordAfterKeyword(message, aliceConstantsService.getVoiceAssistantNames());
                if (aliceConstantsService.getSendVoiceCommands().contains(command)) {
                    return aliceSendService.prepareMessageSending(message);
                }
                if (aliceConstantsService.getReceiveNewsCommands().contains(command)) {
                    return aliceReceiveService.checkNotifications();
                }
                if(aliceConstantsService.getReceiveVoiceCommands().contains(command)){
                    return aliceReceiveService.aliceReadLastMessages(this.getUserFromCommand(message));
                }
            }
            else {
                firstWord = firstWord.toLowerCase();
                if(lastCommandInSessionIs("ASK_TO_ANSWER_TO_MESSAGE")){
                    this.unsetLastCommandFromSession();
                    if(firstWord.equals("да")){
                        this.saveLastCommandToSession("MESSAGE_PREPARED");
                        return this.getListenVoiceMessage();
                    }
                    else if(firstWord.equals("нет")){
                        return this.generateAnswerMessage(" ", true);
                    }
                }
                if (aliceConstantsService.getReceiveVoiceCommands().contains(firstWord)) {
                    return aliceReceiveService.aliceReadLastMessages(this.getUserFromCommand(message));
                }

            }

        } catch (Exception e) {
            response = this.generateAnswerMessage(e.getMessage());
            response.setError(true);
            return  response;
        }
        response  =this.generateAnswerMessage("Ошибка");
        response.setError(true);

        return response;
    }


    public boolean lastCommandInSessionIs(String command){
        String sessionKey = SecurityWorkspace.getAuthUserId().toString();
        String lastCommand = (String) httpSession.getAttribute(sessionKey+"-command");
        if(lastCommand == null){
            return false;
        }
        return lastCommand.equals(command);
    }

    public String getLastCommandFromSession(){
        String sessionKey = SecurityWorkspace.getAuthUserId().toString();
        String command = (String) httpSession.getAttribute(sessionKey+"-command");
        return command ==null ? "" : command;
    }


    public void unsetLastCommandFromSession(){
        String sessionKey = SecurityWorkspace.getAuthUserId().toString();
        httpSession.removeAttribute(sessionKey+"-command");
    }

    public User findFriend(String userName) throws Exception {
        List<User> users = userService.findUsersByName(userName);
        if(users.size() > 1){
            throw new Exception("Пользователей с таким именем несколько ");
        }else if(users.isEmpty()){
            List<Contact> contacts = contactRepository.findByName(userName);
            if(contacts.size() == 1){
                return  contacts.get(0).user;
            }
            throw new Exception("Пользователь с таким именем не найден");
        }
        return users.get(0);
    }

    public User getUserFromCommand(String message) throws Exception {
        String friendName = extractWordAfterKeyword(message, aliceConstantsService.getReadFromTargetUsers());
        String userName = friendName.substring(0, 1).toUpperCase() + friendName.substring(1);
        return findFriend(userName);
    }

    public String extractWordAfterKeyword(String input, List<String> keywords) {
        for (String keyword : keywords) {
            String patternString = keyword + "\\s+(\\S+)";
            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = pattern.matcher(input);
            if (matcher.find()) {
                return matcher.group(1);
            }

        }
        return null;
    }

    public ByteFileResponse generateAnswerMessage(String text, boolean dontNeedAnswer) throws IOException {
        return new ByteFileResponse(speechKitService.synthesizeText(new SynthesizeDto(text)), dontNeedAnswer);
    }

    public ByteFileResponse generateAnswerMessage(String text) throws IOException {
        return new ByteFileResponse(speechKitService.synthesizeText(new SynthesizeDto(text)));
    }


    public ByteFileResponse getListenVoiceMessage() throws IOException {
        return new ByteFileResponse(speechKitService.synthesizeText(new SynthesizeDto("Говорите после сигнала пип sil<[1000]> пииип")));
    }

    public String saveLastCommandToSession(String command){
        String sessionKey = SecurityWorkspace.getAuthUserId().toString();
        httpSession.setAttribute(sessionKey+"-command", command);
        return sessionKey;
    }
}
