package com.example.alice.services.ailce;


import com.example.alice.DTO.message.MessageDTO;
import com.example.alice.DTO.speechKit.SynthesizeDto;
import com.example.alice.models.chats.Message;
import com.example.alice.models.users.User;
import com.example.alice.repos.MessageRepository;
import com.example.alice.repos.NotificationRepository;
import com.example.alice.responses.ByteFileResponse;
import com.example.alice.services.ChatService;
import com.example.alice.services.MessageService;
import com.example.alice.services.files.FileService;
import com.example.alice.services.integrations.SpeechKit.SpeechKitService;
import com.example.alice.services.notification.NotificationService;
import com.example.alice.utilities.SecurityWorkspace;
import jakarta.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AliceReceiveService {

    private HttpSession httpSession;
    private final NotificationService notificationService;
    private final MessageService messageService;
    private final SpeechKitService speechKitService;
    private final ChatService chatService;
    private final FileService fileService;
    private final AliceConstantsService aliceConstantsService;
    private final MessageRepository messageRepository;
    private final NotificationRepository notificationRepository;


    public byte[] findMessageById(long id) {
        return fileService.downloadImage("message "+ id);
    }

    public ByteFileResponse aliceReadLastMessages(User user) throws IOException {
        List<byte[]> userMessages = new ArrayList<>(findUserMessages(user.getName()));

        SynthesizeDto text = new SynthesizeDto(aliceConstantsService.getAskToAnswerToMessageText());
        byte[] answerVoice = new ByteFileResponse(speechKitService.synthesizeText(text)).getVoiceFile();

        userMessages.add(answerVoice);

        try {
            this.prepareMessageForAnswer(user.getName());
            this.saveLastCommandToSession("ASK_TO_ANSWER_TO_MESSAGE");
            return this.voiceListOfMessages(userMessages);
        } catch (Exception exception) {
            return this.getUserNotFoundMessage();
        }
    }

    private ByteFileResponse voiceListOfMessages(List<byte[]> messages) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        for (byte[] array : messages) {
            if (array != null) {
                try {
                    outputStream.write(array);
                } catch (Exception e) {
                }
            }
        }

        return new ByteFileResponse(outputStream.toByteArray());
    }

    public String saveLastCommandToSession(String command){
        String sessionKey = SecurityWorkspace.getAuthUserId().toString();
        httpSession.setAttribute(sessionKey+"-command", command);
        return sessionKey;
    }

    public String saveMessageToSession(MessageDTO messageDTO){
        String sessionKey = SecurityWorkspace.getAuthUserId()+"-messadeDto";
        httpSession.setAttribute(sessionKey, messageDTO);
        return sessionKey;
    }
    public String saveUserToSession(String user){
        String sessionKey = SecurityWorkspace.getAuthUserId()+"-user";
        httpSession.setAttribute(sessionKey, user);
        return sessionKey;
    }

    public List<byte[]> findUserMessages(String userName) throws IOException {

        return messageRepository.findByIsVoiceTrueAndChat_Users_IdAndViewedFalse(SecurityWorkspace.getAuthUserId())
                .stream().filter(message -> Objects.equals(message.getUser().getName(), userName))
                .map(message -> {
                    if(message.getAttachment() != null) {
                        byte[] audio = fileService.downloadImage("message "+ message.getId());
                        if(audio != null) {
                            notificationService.findAndReadNotificationByMessage(message);
                            messageService.readMessage(message);
                          return audio;
                        }
                    }
                    return null;
                })
                .toList();
    }





    public ByteFileResponse checkNotifications() throws IOException {

        List<Message> messages = messageRepository.findByIsVoiceTrueAndChat_Users_IdAndViewedFalse(SecurityWorkspace.getAuthUserId())
                .stream().filter(message -> !Objects.equals(message.getUser().getId(), SecurityWorkspace.getAuthUserId())).toList();
        return this.getNewNotificationMessage(messages);
    }


    public String getAllNotificationSenders(List<Message> messages){
        return messages.stream()
                .map(message ->
                    message.getUser().getName()
                )
                .distinct()
                .collect(Collectors.joining(", "));
    }

    public ByteFileResponse getNewNotificationMessage(List<Message> messages) throws IOException {
        if(!messages.isEmpty()) {
            String senders = this.getAllNotificationSenders(messages);
            String[] sendersCount = senders.split(",");
            String text;
            if(sendersCount.length == 1){
                text = aliceConstantsService.getOneUnreadedMessagesText();
                if(Objects.equals(sendersCount[0], aliceConstantsService.getMom())) {
                     text += "sil<[100]> Мамы. sil<[200]> ";
                }else{
                    text += "контакта" + sendersCount[0] + ". ";
                }
                this.saveUserToSession(sendersCount[0]);
                this.saveLastCommandToSession("ASK_TO_LISTEN_MESSAGE");
                text += aliceConstantsService.getAskToListenMessageText();
            }else{
                text = aliceConstantsService.getSomeUnreadedMessagesText() + senders + aliceConstantsService.getWhoToAskToListenMessageText();
                this.saveLastCommandToSession("ASK_WHO_TO_LISTEN_MESSAGE");
            }

            return new ByteFileResponse(speechKitService.synthesizeText(new SynthesizeDto(text)));
           }else{
            return new ByteFileResponse(speechKitService.synthesizeText(new SynthesizeDto(aliceConstantsService.getNoNotificationsMessageText())), true);
        }

    }

    public void prepareMessageForAnswer(String userName){
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setUser(SecurityWorkspace.getAuthUser());
        messageDTO.setVoice(true);
        messageDTO.setChatId(chatService.findByTitle(userName, SecurityWorkspace.getAuthUser().getName()));
        this.saveMessageToSession(messageDTO);
    }



    public ByteFileResponse getUserNotFoundMessage() throws IOException {
        return new ByteFileResponse(speechKitService.synthesizeText(new SynthesizeDto(aliceConstantsService.getUserNotFoundMessageText())));
    }


}
