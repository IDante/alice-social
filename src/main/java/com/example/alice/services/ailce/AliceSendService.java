package com.example.alice.services.ailce;


import com.amazonaws.util.json.Jackson;
import com.example.alice.DTO.message.MessageDTO;
import com.example.alice.DTO.speechKit.SynthesizeDto;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.EmotionEnum;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.VoiceEnum;
import com.example.alice.exceptions.AliceNotFoundException;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.models.FileResponse;
import com.example.alice.models.chats.Message;
import com.example.alice.models.users.User;
import com.example.alice.responses.ByteFileResponse;
import com.example.alice.services.ChatService;
import com.example.alice.services.MessageService;
import com.example.alice.services.UserService;
import com.example.alice.services.files.FileService;
import com.example.alice.services.integrations.SpeechKit.SpeechKitService;
import com.example.alice.utilities.SecurityWorkspace;
import jakarta.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
public class AliceSendService{

    private final MessageService messageService;
    private final ChatService chatService;
    private final SpeechKitService speechKitService;
    private final UserService userService;
    private HttpSession httpSession;
    private final FileService fileService;
    private final AliceConstantsService aliceConstantsService;


    private Message aliceSendVoice(MessageDTO messageDTO, long chatId) {
        return messageService.create(messageDTO, chatId);
    }

    public ByteFileResponse prepareMessageSending(String message) throws Exception {
        ByteFileResponse response = this.getListenVoiceMessage();
        String voice = extractWordAfterKeyword(message, aliceConstantsService.getSendVoiceCommands());

        boolean isVoice = false;
        if(voice!=null) {
            isVoice = isVoiceCheck(voice, aliceConstantsService.getVoiceNames());
        }

        String friendName = extractWordAfterKeyword(message, aliceConstantsService.getTargetUsers());
        if (friendName ==null) {
            friendName = extractWordAfterKeyword(message, aliceConstantsService.getListForNameSearch());
            if(friendName == null){
                throw new Exception(aliceConstantsService.getUserNotFoundMessageText());
            }
            if(aliceConstantsService.getAvailableNames().contains(friendName)){
                friendName = aliceConstantsService.getMom();
            }
        }
        friendName = friendName.substring(0, 1).toUpperCase() + friendName.substring(1);


        User user = null;
        long chatId = 0;
        try {
            user = userService.findById(SecurityWorkspace.getAuthUserId());
            chatId = chatService.findByTitle(user.getName(), friendName);
        }catch (Exception exception){
            throw new NotFoundException("������������ � ����� ������ �� ������");
        }

        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setUser(user);
        messageDTO.setVoice(isVoice);
        messageDTO.setChatId(chatId);

        this.saveMessageToSession(messageDTO);
        this.unsetLastCommandFromSession();
        this.saveLastCommandToSession("MESSAGE_PREPARED");
        response.setVoiceFile(this.getListenVoiceMessage().getVoiceFile());

        return response;
    }

    public ByteFileResponse sendMessage(MultipartFile voiceFile) throws IOException {
        String text = Jackson.stringMapFromJsonString(speechKitService
                .recognizeText(voiceFile.getBytes())).get("result");

        String sessionKey = SecurityWorkspace.getAuthUserId()+"-messadeDto";

        MessageDTO messageDTO = (MessageDTO) httpSession.getAttribute(sessionKey);
        httpSession.removeAttribute(sessionKey);
        this.unsetLastCommandFromSession();
        messageDTO.setMessageText(text);
        
        try {
            Message message = aliceSendVoice(messageDTO, messageDTO.getChatId());
            FileResponse file = fileService.saveFile("message " + message.getId(), "", voiceFile);
            message.setAttachment(file.getFullUrl());

            messageService.updateOrSaveMessage(message);
            return this.getSuccessVoiceMessage();
        } catch (AliceNotFoundException ex) {
            return this.getErrorVoiceMessage();
        }
    }

    public String extractWordAfterKeyword(String input, List<String> keywords) {
        for (String keyword : keywords) {
            String patternString = keyword + "\\s+(\\S+)";
            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = pattern.matcher(input);
            if (matcher.find()) {
                return matcher.group(1);
            }
        }
        return null;
    }

    private boolean isVoiceCheck(String voice, List<String> voiceNames) {
        for (String i : voiceNames) {
            if (voice.equals(i)) {
                return true;
            }
        }
        return false;
    }

    public String saveMessageToSession(MessageDTO messageDTO){
        String sessionKey = SecurityWorkspace.getAuthUserId()+"-messadeDto";
        httpSession.setAttribute(sessionKey, messageDTO);
        return sessionKey;
    }

    public String saveLastCommandToSession(String command){
        String sessionKey = SecurityWorkspace.getAuthUserId().toString();
        httpSession.setAttribute(sessionKey+"-command", command);
        return sessionKey;
    }
    
    public void unsetLastCommandFromSession(){
        String sessionKey = SecurityWorkspace.getAuthUserId().toString();
        httpSession.removeAttribute(sessionKey+"-command");
    }

    public ByteFileResponse getListenVoiceMessage() throws IOException {
        return new ByteFileResponse(speechKitService.synthesizeText(new SynthesizeDto(aliceConstantsService.getAssistantIsListeningMessage())));
    }

    public ByteFileResponse getErrorVoiceMessage() throws IOException {
        return new ByteFileResponse(speechKitService.synthesizeText(new SynthesizeDto("��������� ������")));
    }

    public ByteFileResponse getSuccessVoiceMessage() throws IOException {
        SynthesizeDto synthesizeDto = new SynthesizeDto(aliceConstantsService.getSuccessMessageText());
        synthesizeDto.setEmotion(EmotionEnum.GOOD);
        return new ByteFileResponse(speechKitService.synthesizeText(synthesizeDto), true);
    }

}
