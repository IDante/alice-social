package com.example.alice.services.contacts;

import com.example.alice.DTO.contact.ContactDTO;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.models.chats.Chat;
import com.example.alice.models.contacts.Contact;
import com.example.alice.models.users.User;
import com.example.alice.repos.UserRepository;
import com.example.alice.repos.contacts.ContactRepository;
import com.example.alice.requests.contact.ContactCreateRequest;
import com.example.alice.services.UserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class ContactService {
    private final ContactRepository contactRepository;
    private final UserService userService;

    public List<Contact> findUsersContacts(Long userId) {
        return contactRepository.findByUser_Id(userId);
    }

    public List<ContactDTO> findUsersContactList(Long userId) {
        User user = userService.findById(userId);
        return user.getContactBook()
                .stream()
                .map(
                        (Contact contact) -> new ContactDTO(
                                contact,
                                user.getFriends().contains(contact.getUser()),
                                user.getChats().stream().filter(
                                        (Chat chat) -> new HashSet<>(chat.getUsers()).containsAll(List.of(user, contact.getUser()))
                                ).findFirst().orElse(null)
                        )
                )
                .toList();
    }

    @Transactional
    public Contact create(ContactCreateRequest contactCreateRequest, Long userId) {
        Contact contact = new Contact();
        contact.setName(contactCreateRequest.getName());
        contact.setTag(contactCreateRequest.getTag());
        contact.setPhone(contactCreateRequest.getPhone());
        contact.setUser(userService.findById(userId));

        return contactRepository.save(contact);
    }

    @Transactional
    public Contact update(Long contactId, ContactDTO contactDTO) {
        Contact contact = this.findById(contactId);

        contact.setPhone(contactDTO.getPhone());
        contact.setName(contactDTO.getName());
        contact.setTag(contactDTO.getTag());

        return contactRepository.save(contact);
    }

    public Contact findById(Long id) {
        return contactRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No such contact"));
    }


    @Transactional
    public void deleteById(Long id) {
        try {
            contactRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new NotFoundException("No such contact");
        }
    }

    public void addContactToUser(Contact contact, User user) {
        user.getContactBook().add(contact);
        userService.update(user, new UserDTO(user));
    }
}
