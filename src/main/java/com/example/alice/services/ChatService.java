package com.example.alice.services;

import com.example.alice.DTO.message.MessageDTO;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.exceptions.AliceNotFoundException;
import com.example.alice.models.users.User;
import com.example.alice.repos.UserRepository;
import com.example.alice.responses.MessagesResponse;
import com.example.alice.exceptions.NotFoundException;
import com.example.alice.exceptions.ValidationException;
import com.example.alice.models.chats.Chat;
import com.example.alice.repos.ChatRepository;
import com.example.alice.utilities.SecurityWorkspace;
import lombok.AllArgsConstructor;
import net.minidev.json.JSONUtil;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class ChatService {
    private final ChatRepository chatRepository;
    private final UserRepository userRepository;

    @Transactional
    public Chat create(String title) {
        if (title == null || title.isEmpty()) {
            throw new ValidationException("New title is empty");
        }
        Chat chat = new Chat();
        chat.setTitle(title);
        return chatRepository.save(chat);
    }

    @Transactional
    public Chat create(String title, Collection<User> users) {
        if (title == null || title.isEmpty()) {
            throw new ValidationException("New title is empty");
        }
        Chat chat = new Chat();
        chat.setTitle(title);
        chat = chatRepository.save(chat);
        Chat finalChat = chat;
        users.forEach((User user) -> {
            user.getChats().add(finalChat);
            userRepository.save(user);
        });
        return chat;
    }

    @Transactional
    public Chat update(Chat chat, String newTitle) {
        if (newTitle == null || newTitle.isEmpty()) {
            throw new ValidationException("New title is empty");
        }
        chat.setTitle(newTitle);
        return chatRepository.save(chat);
    }

    public Chat findById(Long id) {
        return chatRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No such chat"));
    }

    public Chat findByUsers(Long firstId, Long secondId) {
        User first = userRepository.findById(firstId)
                .orElseThrow(() -> new NotFoundException("No such user"));
        User second = userRepository.findById(secondId)
                .orElseThrow(() -> new NotFoundException("No such user"));

        return chatRepository.findAll()
                .stream().filter((Chat chat) -> new HashSet<>(chat.getUsers()).containsAll(Set.of(first, second)))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("No such chat"));
    }

    public long findByTitle(String nameOne, String nameSecond){

        String name1 = nameOne + " and " + nameSecond;
        String name2 = nameSecond + " and "  + nameOne;

        Optional<Chat> chat = chatRepository.findByTitle(name1);
        if(chat.isEmpty()){
            chat = chatRepository.findByTitle(name2);
        }

        if(chat.isEmpty()){
            throw new NotFoundException("No such user-friend");
        }
        return chat.get().getId();
    }

    @Transactional
    public void deleteById(Long id) {
        try {
            chatRepository.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new NotFoundException("No such chat");
        }
    }

    public MessagesResponse getChatMessages(Long chatId) {
        Chat chat = findById(chatId);
        if (chat.getUsers().stream().noneMatch((User user) -> Objects.equals(SecurityWorkspace.getAuthUserId(), user.getId()))) {
            throw new AccessDeniedException("Access denied!");
        }
        ModelMapper mapper = new ModelMapper();
        return new MessagesResponse(chat.getMessages()
                .stream()
                .map((element) -> mapper.map(element, MessageDTO.class))
                .peek((element) -> element.setUserDTO(new UserDTO(element.getUser())))
                .toList());
    }
}
