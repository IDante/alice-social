package com.example.alice.services.auth;

import com.example.alice.requests.register.RegistrationRequest;
import com.example.alice.responses.AuthResponse;
import com.example.alice.requests.auth.AuthEmailRequest;
import com.example.alice.models.users.Role;
import com.example.alice.models.users.User;
import com.example.alice.repos.TokenRepository;
import com.example.alice.repos.UserRepository;
import com.example.alice.utilities.Token.Token;
import com.example.alice.utilities.Token.TokenType;
import com.example.alice.yandex.api.Yandex;
import com.example.alice.yandex.api.YandexPassport;
import com.example.alice.yandex.connect.YandexServiceProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository repository;
    private final TokenRepository tokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final YandexServiceProvider provider;

    public AuthResponse register(RegistrationRequest request) {
        var user = User.builder()
                .name(request.getName())
                .surname(request.getSurname())
                .midname(request.getMidname())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .build();
        var savedUser = repository.save(user);
        var jwtToken = jwtService.generateToken(user);
        saveUserToken(savedUser, jwtToken);
        return AuthResponse.builder()
                .token(jwtToken)
                .id(savedUser.getId())
                .build();
    }

    public AuthResponse authenticateEmail(AuthEmailRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = repository.findByEmail(request.getEmail())
                .orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);
        return AuthResponse.builder()
                .token(jwtToken)
                .id(user.getId())
                .build();
    }

    private void saveUserToken(User user, String jwtToken) {
        var token = Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepository.save(token);
    }

    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

    public AuthResponse authYandex(String token) {
        Yandex yandex = provider.getApi(token);
        YandexPassport passport = yandex.userOperations().getPassport();
        User savedUser;
        Optional<User> existingUser = repository.findByEmail(passport.getDefaultEmail());
        if (existingUser.isPresent()){
            savedUser = existingUser.get();
        }
        else{
            var user = new User()
                    .setName(passport.getFirstName())
                    .setSurname(passport.getLastName())
                    .setEmail(passport.getDefaultEmail())
                    .setRole(Role.USER)
                    .setAvatarUrl(getAvatarUrl(passport))
                    ;
            savedUser = repository.save(user);
        }
        var jwtToken = jwtService.generateToken(savedUser);
        saveUserToken(savedUser, jwtToken);
        return AuthResponse.builder()
                .token(jwtToken)
                .id(savedUser.getId())
                .build();
    }

    private String getAvatarUrl(YandexPassport passport) {
        String id = passport.getDefaultAvatarId();
        return "https://avatars.yandex.net/get-yapic/"+id+"/"+"islands-200";
    }

}
