package com.example.alice.services;

import com.example.alice.exceptions.QrCodeException;
import com.github.aytchell.qrgen.QrConfigurationException;
import com.github.aytchell.qrgen.QrGenerationException;
import com.github.aytchell.qrgen.QrGenerator;
import com.github.aytchell.qrgen.colors.RgbValue;
import com.github.aytchell.qrgen.config.ErrorCorrectionLevel;
import com.github.aytchell.qrgen.config.ImageFileType;
import com.github.aytchell.qrgen.config.MarkerStyle;
import com.github.aytchell.qrgen.config.PixelStyle;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

@Service
public class QrService {
    private final QrGenerator generator;

    public QrService() {
        try {
            generator  = new com.github.aytchell.qrgen.QrGenerator()
                    .withSize(512, 512)
                    .withMargin(1)
                    .as(ImageFileType.PNG)
                    .withErrorCorrection(ErrorCorrectionLevel.H)
                    .withLogo(getClass().getResourceAsStream("/images/logo.png"))
                    .withPixelStyle(PixelStyle.WATER)
                    .withMarkerStyle(MarkerStyle.ROUND_CORNERS);
            final RgbValue white = new RgbValue(0xffffff);
            final RgbValue black = new RgbValue(0X000000);
            generator.withColors(
                    black,              // 'pixel' color
                    white,              // background color
                    black,        // outer marker color
                    black);    // inner marker color
        } catch (IOException | QrConfigurationException e) {
            throw new QrCodeException("Error initializing qr code creator");
        }
    }

    public byte[] generateQRWithLogo(String barcodeText) {
        Path file;
        try {
            file = generator.writeToTmpFile(barcodeText);
        } catch (IOException | QrGenerationException e) {
            throw new QrCodeException("Couldn't generate qr-code for: "+barcodeText);
        }
        File code = new File(file.toUri());
        try {
            BufferedImage image = ImageIO.read(code);
            if (!code.delete()){
                throw new QrCodeException("Couldn't delete temp qr-code file: "+code.getName());
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write(image, "png", baos);
            } catch (IOException e) {
                throw new QrCodeException("Error sending qr-code image response: "+barcodeText);
            }
            return baos.toByteArray();
        } catch (IOException e) {
            throw new QrCodeException("Couldn't read qr-code for: "+barcodeText);
        }
    }


    public byte[] generateEasterEggQr(String barcodeText) {
        try {
            generator.withLogo(getClass().getResourceAsStream("/images/easter-egg.png"));
            byte [] code = generateQRWithLogo(barcodeText);
            generator.withLogo(getClass().getResourceAsStream("/images/logo.png"));
            return code;
        } catch (IOException e) {
            throw new QrCodeException("Couldn't get easter egg❓");
        }
    }
}
