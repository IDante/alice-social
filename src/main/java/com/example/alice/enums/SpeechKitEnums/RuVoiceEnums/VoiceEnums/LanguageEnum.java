package com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums;

import lombok.Getter;

@Getter
public enum LanguageEnum {

    RU ("ru-RU"),
    KZ ("kk-KK"),
    EN ("en-US");

    private final String languageName;

    LanguageEnum(String languageName) {
        this.languageName = languageName;
    }
}
