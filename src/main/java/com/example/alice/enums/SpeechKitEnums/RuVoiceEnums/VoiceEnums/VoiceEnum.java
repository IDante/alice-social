package com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums;

import lombok.Getter;

import java.util.Objects;


@Getter
public enum VoiceEnum {

    ALENA ("alena"),
    ERMIL ("ermil"),
    MADIRUS ("madirus"),
    ZAHAR ("zahar") {
        @Override
        public boolean supportsEmotion(EmotionEnum emotionEnum){
            switch (emotionEnum) {
                case NEUTRAL, GOOD -> {
                    return true;
                }
            }
            return false;
        }
    },
    JANE ("jane") {
        @Override
        public boolean supportsEmotion(EmotionEnum emotionEnum){
            switch (emotionEnum) {
                case NEUTRAL, GOOD, EVIL -> {
                    return true;
                }
            }
            return false;
        }
    },
    OMAZH ("omazh") {
        @Override
        public boolean supportsEmotion(EmotionEnum emotionEnum){
            switch (emotionEnum) {
                case NEUTRAL, EVIL -> {
                    return true;
                }
            }
            return false;
        }
    },
    JOHN ("filipp"){
        @Override
        public LanguageEnum getLanguage() {
            return LanguageEnum.EN;
        }
    };



    public String getEmotion() {
        return "neutral";
    }

    public boolean supportsEmotion(EmotionEnum emotionEnum){
       return Objects.requireNonNull(emotionEnum) == EmotionEnum.NEUTRAL;
    }

    public LanguageEnum getLanguage() {
        return LanguageEnum.RU;
    }


    private final String name;

    VoiceEnum(String name) {
        this.name = name;
    }
}
