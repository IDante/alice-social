package com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums;


import lombok.Getter;

@Getter
public enum EmotionEnum {

    NEUTRAL ("neutral"),
    GOOD ("good"),
    EVIL ("evil");

    public String emotionName;

    EmotionEnum(String emotion) {
        this.emotionName = emotion;
    }

}
