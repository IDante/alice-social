package com.example.alice.responses;

import com.example.alice.DTO.notification.NotificationWithoutUserDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class NotificationsResponse {
    @JsonProperty("notification")
    @NotEmpty
    List<@Valid NotificationWithoutUserDTO> notification;
}
