package com.example.alice.responses;

import com.example.alice.DTO.contact.ContactDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class ContactsResponse {


    @JsonProperty("contacts")
    private List<ContactDTO> contactDTOList;


}
