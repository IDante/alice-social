package com.example.alice.responses;

import com.example.alice.DTO.user.UserDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class UsersResponse {
    @JsonProperty("users")
    @NotEmpty
    List<@Valid UserDTO> users;
}
