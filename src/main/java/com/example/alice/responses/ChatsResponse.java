package com.example.alice.responses;

import com.example.alice.DTO.chat.ChatDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;

import java.util.List;


@Schema(description = "Ответ с чатами пользователя")
@AllArgsConstructor
public class ChatsResponse {
    @Schema(description = "Список чатов")
    @JsonProperty("chats")
    @NotEmpty
    List<@Valid ChatDTO> chats;
}
