package com.example.alice.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Ответ авторизации")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthResponse {
    @Schema(description = "JWT токен авторизации", example = "какой-то очень длинный текст токена")
    @JsonProperty("token")
    private String token;
    @Schema(description = "Идентификатор пользователя", example = "1")
    private Long id;

}
