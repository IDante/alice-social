package com.example.alice.responses;

import com.example.alice.DTO.message.MessageDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class MessagesResponse {
    @JsonProperty("messages")
    @NotEmpty
    List<@Valid MessageDTO> messages;
}
