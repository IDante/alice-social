package com.example.alice.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Schema(description = "Ответ с голосовым сообщением")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ByteFileResponse {
    @JsonProperty("voiceFile")
    byte[] voiceFile;

    @JsonIgnore
    boolean isError;

    public ByteFileResponse(byte[] voiceFile) {
        this.voiceFile = voiceFile;
    }
}
