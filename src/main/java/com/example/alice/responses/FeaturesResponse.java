package com.example.alice.responses;

import com.example.alice.DTO.feature.FeatureDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class FeaturesResponse {
    @JsonProperty("features")
    List<@Valid FeatureDTO> features;
}
