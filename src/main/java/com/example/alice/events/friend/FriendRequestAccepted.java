package com.example.alice.events.friend;

import com.example.alice.models.users.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class FriendRequestAccepted extends FriendRequest {
    public FriendRequestAccepted(Object source, User from, User to) {
        super(source, from, to);
    }
}
