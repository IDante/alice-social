package com.example.alice.events.friend;

import com.example.alice.models.users.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendRequestSent extends FriendRequest {
    public FriendRequestSent(Object source, User from, User to) {
        super(source, from, to);
    }
}
