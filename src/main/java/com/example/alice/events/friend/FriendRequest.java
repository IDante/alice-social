package com.example.alice.events.friend;

import com.example.alice.models.users.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
abstract public class FriendRequest extends ApplicationEvent {
    private User from;
    private User to;

    public FriendRequest(Object source, User from, User to) {
        super(source);
        this.from = from;
        this.to = to;
    }
}
