package com.example.alice.events.message;

import com.example.alice.models.chats.Message;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class MessageCreated extends ApplicationEvent {
    private Message message;

    public MessageCreated(Object source, Message message) {
        super(source);
        this.message = message;
    }
}
