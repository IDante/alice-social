package com.example.alice.strategies;

import com.example.alice.models.chats.Chat;
import com.example.alice.models.users.User;
import com.example.alice.services.ChatService;
import com.example.alice.services.FriendService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class FriendRequestAcceptedStrategy implements FriendRequestStrategy {
    private final ChatService chatService;

    @Override
    public void proceedRequest(User from, User to) {
        chatService.create(from.getName() + " and " + to.getName(), List.of(from, to));
    }
}
