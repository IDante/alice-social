package com.example.alice.strategies;

import com.example.alice.models.users.User;

public interface FriendRequestStrategy {
    public void proceedRequest(User from, User to);
}
