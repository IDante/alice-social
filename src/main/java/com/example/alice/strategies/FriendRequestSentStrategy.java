package com.example.alice.strategies;

import com.example.alice.DTO.notification.NotificationDTO;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.NotificationType;
import com.example.alice.models.users.User;
import com.example.alice.services.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class FriendRequestSentStrategy implements FriendRequestStrategy {
    private NotificationService notificationService;

    @Override
    public void proceedRequest(User from, User to) {
        NotificationDTO notificationDTO = new NotificationDTO(
                to,
                NotificationType.FRIEND_REQUEST,
                "Запрос в друзья",
                "{\"user_id\": " + from.getId() +
                        ", \"surname\": \"" + from.getSurname() +
                        "\", \"name\": \"" + from.getName() +
                        "\", \"avatar_url\": \"" + from.getAvatarUrl() + "\"}",
                false
        );
        notificationService.create(notificationDTO);
    }
}
