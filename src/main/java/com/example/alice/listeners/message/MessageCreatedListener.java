package com.example.alice.listeners.message;

import com.example.alice.DTO.notification.NotificationDTO;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.NotificationType;
import com.example.alice.events.message.MessageCreated;
import com.example.alice.models.chats.Chat;
import com.example.alice.models.chats.Message;
import com.example.alice.models.users.User;
import com.example.alice.services.integrations.PusherService;
import com.example.alice.services.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

@Component
@AllArgsConstructor
public class MessageCreatedListener implements ApplicationListener<MessageCreated> {
    private final PusherService pusherService;
    private final NotificationService notificationService;

    @Override
    public void onApplicationEvent(MessageCreated event) {
        Message message = event.getMessage();
        Map<String, String> jsonResponse = Map.of(
                "text", message.getMessageText(),
                "from", String.valueOf(message.getUser().getId())
        );
        addNotificationToAllInChat(message.getId(), message.getChat(), message.getUser());
        pusherService.send("chat-" + message.getChat().getId(), "message-send", jsonResponse);
    }

    private void addNotificationToAllInChat(Long messageId, Chat chat, User sender) {
        chat.getUsers().stream().filter((User user) -> !Objects.equals(user.getId(), sender.getId()))
                .forEach((User user) -> {
                    NotificationDTO notificationDTO = new NotificationDTO(
                            user,
                            NotificationType.MESSAGE,
                            "Отправлено сообщение",
                            "{\"message_id\": " + messageId + "}",
                            false
                    );
                    notificationService.create(notificationDTO);
                });
    }
}
