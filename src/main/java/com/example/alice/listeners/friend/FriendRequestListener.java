package com.example.alice.listeners.friend;

import com.example.alice.events.friend.FriendRequest;
import com.example.alice.events.friend.FriendRequestSent;
import com.example.alice.services.ChatService;
import com.example.alice.services.notification.NotificationService;
import com.example.alice.strategies.FriendRequestAcceptedStrategy;
import com.example.alice.strategies.FriendRequestSentStrategy;
import com.example.alice.strategies.FriendRequestStrategy;
import jakarta.annotation.Nonnull;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class FriendRequestListener implements ApplicationListener<FriendRequest> {
    private final NotificationService notificationService;
    private final ChatService chatService;

    @Override
    public void onApplicationEvent(@Nonnull FriendRequest event) {
        FriendRequestStrategy friendRequestStrategy;
        if (event instanceof FriendRequestSent) {
            friendRequestStrategy = new FriendRequestSentStrategy(notificationService);
        } else {
            friendRequestStrategy = new FriendRequestAcceptedStrategy(chatService);
        }

        friendRequestStrategy.proceedRequest(event.getFrom(), event.getTo());
    }
}
