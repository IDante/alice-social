package com.example.alice.validators;

import com.example.alice.DTO.contact.ContactDTO;
import com.example.alice.DTO.user.UserDTO;
import com.example.alice.exceptions.ValidationException;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class ContactDTOValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ContactDTO contactDTO = (ContactDTO) target;
        if(contactDTO.getName() == null || contactDTO.getName().isEmpty()){
            throw new ValidationException("You should enter contact name");
        }
        if(contactDTO.getPhone().length() < 10){
            throw new ValidationException("You phone is too short");
        }
        if(contactDTO.getPhone().length() > 10){
            throw new ValidationException("You phone is too long");
        }
    }
}