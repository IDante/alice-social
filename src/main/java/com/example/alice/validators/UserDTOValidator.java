package com.example.alice.validators;

import com.example.alice.DTO.user.UserDTO;
import com.example.alice.exceptions.ValidationException;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.apache.commons.validator.routines.EmailValidator;

@Service
public class UserDTOValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDTO userDTO = (UserDTO) target;
        if(userDTO.getName() == null || userDTO.getName().isEmpty()){
            throw new ValidationException("You should enter your name");
        }
        if(userDTO.getName().length() > 50){
            throw new ValidationException("You name is too long");
        }
        if(userDTO.getSurname() == null || userDTO.getSurname().isEmpty()){
            throw new ValidationException("You should enter your Surname");
        }
        if(userDTO.getSurname().length() > 50){
            throw new ValidationException("You Surname is too long");
        }
        if(!EmailValidator.getInstance().isValid(userDTO.getEmail())){
            throw new ValidationException("You should enter your email");
        }
    }
}