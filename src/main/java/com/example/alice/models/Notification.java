package com.example.alice.models;

import com.example.alice.enums.SpeechKitEnums.RuVoiceEnums.VoiceEnums.NotificationType;
import com.example.alice.models.users.User;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "notifications")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "notification_type")
    private NotificationType notificationType;

    @NotEmpty
    @Column(name = "notification_text")
    private String notificationText;

    @Lob
    @Column(name = "body")
    @JdbcTypeCode(SqlTypes.JSON)
    private String body;

    @Column(name = "viewed")
    private Boolean viewed;

}