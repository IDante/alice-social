package com.example.alice.models.chats;

import com.example.alice.models.users.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "chats")
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title")
    private String title;

    @ManyToMany(mappedBy = "chats")
    private List<User> users = new ArrayList<>();

    @OneToMany(mappedBy = "chat")
    @OrderBy("id DESC")
    private List<Message> messages = new ArrayList<>();

    public void addToUsers(User user){
        users.add(user);
    }

    public Message getLastMessage(){
        if (!messages.isEmpty()) {
            return messages.get(0);
        }
        return null;
    }
}