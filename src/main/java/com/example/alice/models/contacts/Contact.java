package com.example.alice.models.contacts;

import com.example.alice.models.users.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "contacts")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "phone", unique = true)
    private String phone;

    @Column(name = "name")
    private String name;

    @Column(name = "tag")
    private String tag;

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User user;

    @Column(name = "has_smart_home")
    private Boolean hasSmartHome = false;

    @Column(name = "has_smart_speaker")
    private Boolean hasSmartSpeaker = false;

}