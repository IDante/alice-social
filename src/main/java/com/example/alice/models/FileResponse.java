package com.example.alice.models;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="file")
public class FileResponse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "title")
    private String title;

    @Column(name =  "description")
    private String description;

    @Column(name = "path")
    private String imagePath;

    @Column(name = "file")
    private String imageFileName;

    @Column(name = "url")
    private String fullUrl;

}