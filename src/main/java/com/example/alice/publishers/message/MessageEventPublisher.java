package com.example.alice.publishers.message;

import com.example.alice.events.message.MessageCreated;
import com.example.alice.models.chats.Message;
import com.example.alice.publishers.EventPublisher;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class MessageEventPublisher extends EventPublisher {

    public MessageEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        super(applicationEventPublisher);
    }

    public void publishCreated(final Message message){
        MessageCreated messageEvent = new MessageCreated(this, message);
        publish(messageEvent);
    }
}
