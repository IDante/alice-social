package com.example.alice.publishers;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
abstract public class EventPublisher {
    private ApplicationEventPublisher applicationEventPublisher;

    protected void publish(ApplicationEvent event){
        applicationEventPublisher.publishEvent(event);
    }
}
