package com.example.alice.configs;

import com.example.alice.yandex.connect.YandexServiceProvider;
import com.nimbusds.jose.util.Pair;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class YandexConfig {
    @Value("${auth.yandex.client.id}")
    private String clientId;

    @Value("${auth.yandex.client.secret}")
    private String clientSecret;

    @Value("${auth.yandex.cloud.folder.id}")
    private String folderId;

    @Value("${auth.yandex.api.key}")
    private String apiKey;

    @Bean
    public YandexServiceProvider yandexServiceProvider() {
        return new YandexServiceProvider(clientId, clientSecret);
    }

    @Bean
    public Pair<String, String> getAuthProps() {
        return Pair.of(apiKey, folderId);
    }

}
