package com.example.alice.configs;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BucketName {
    BUCKET_IMAGE("alice-storage");
    private final String bucketName;
}
