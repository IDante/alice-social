package com.example.alice.configs;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        servers = @Server(url = "alisasharedemo.ru", description = "сайт нашего проекта"),
        info = @Info(
                title = "Алиса, поделись",
                description = "Проект, разрабатываемый распределенной командой №5 на базе хаба в Алматы", version = "1.0.0",
                contact = @Contact(
                        name = "Backend: FMLM - Fedya, Max, Lev and Max",
                        url = "https://t.me/vonpartridge"
                )

        )
)
public class OpenApiConfig {

}
