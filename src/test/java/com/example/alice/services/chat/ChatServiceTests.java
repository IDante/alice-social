package com.example.alice.services.chat;

import com.example.alice.exceptions.NotFoundException;
import com.example.alice.exceptions.ValidationException;
import com.example.alice.models.chats.Chat;
import com.example.alice.models.users.User;
import com.example.alice.repos.ChatRepository;
import com.example.alice.repos.UserRepository;
import com.example.alice.services.ChatService;
import com.example.alice.utilities.SecurityWorkspace;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ChatServiceTests {
    @InjectMocks
    private ChatService chatService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private ChatRepository chatRepository;

    @Test
    public void testCreate() {
        Chat chat = new Chat();
        chat.setTitle("123");
        when(chatRepository.save(any())).thenReturn(chat);

        Chat result = chatService.create("123");
        assertEquals("123", result.getTitle());
        verify(chatRepository).save(any());
    }

    @Test
    public void testCreateError() {
        assertThrows(ValidationException.class, () -> chatService.create(""));
        assertThrows(ValidationException.class, () -> chatService.create(null));
    }

    @Test
    public void testUpdate() {
        Chat chat = new Chat();
        chat.setTitle("123");
        when(chatRepository.save(any())).thenReturn(chat);

        Chat result = chatService.create("123");
        assertEquals("123", result.getTitle());

        Chat updated = chatService.update(result, "1234");
        assertEquals("1234", updated.getTitle());

        verify(chatRepository, times(2)).save(any());
    }

    @Test
    public void testUpdateError() {
        Chat chat = new Chat();
        chat.setTitle("123");
        when(chatRepository.save(any())).thenReturn(chat);

        Chat result = chatService.create("123");
        assertEquals("123", result.getTitle());

        assertThrows(ValidationException.class, () -> chatService.update(result, ""));
        assertThrows(ValidationException.class, () -> chatService.update(result, null));
    }

    @Test
    public void testFindById() {
        Chat chat = new Chat();
        chat.setTitle("123");
        when(chatRepository.findById(any())).thenReturn(Optional.of(chat));

        Chat result = chatService.findById(1L);
        assertEquals("123", result.getTitle());
    }

    @Test
    public void testFindByIdError() {
        Chat chat = new Chat();
        chat.setTitle("123");
        when(chatRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> chatService.findById(1L));
    }

    public long findByTitle(String nameOne, String nameSecond) {
        Chat chat = chatRepository.findByTitle(nameOne + " and " + nameSecond)
                .orElse(chatRepository.findByTitle(nameSecond + " and " + nameOne).orElse(null));
        if (chat == null) {
            throw new NotFoundException("No such user-friend");
        }
        return chat.getId();
    }

    @Test
    @WithMockUser(authorities = {"USER"}, username = "username", password = "123123")
    public void testGetChatMessages() {
        User user1 = new User();
        user1.setId(1L);
        User user2 = new User();
        user2.setId(2L);

        Chat chat = new Chat();
        chat.setTitle("123");
        chat.setUsers(List.of(
                user1, user2
        ));
        when(chatRepository.findById(any())).thenReturn(Optional.of(chat));
        if (chat.getUsers().stream().noneMatch((User user) -> Objects.equals(SecurityWorkspace.getAuthUserId(), user.getId()))) {
            throw new AccessDeniedException("Access denied!");
        }
    }

}
