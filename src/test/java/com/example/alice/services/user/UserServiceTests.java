package com.example.alice.services.user;

import com.example.alice.DTO.user.UserDTO;
import com.example.alice.exceptions.ValidationException;
import com.example.alice.models.users.User;
import com.example.alice.repos.UserRepository;
import com.example.alice.services.UserService;
import com.example.alice.validators.UserDTOValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.validation.DataBinder;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {
    @InjectMocks
    private UserService userService;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserDTOValidator userDTOValidator;

    private static User setUserFields(String name, String surname, String midname, String email){
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setMidname(midname);
        user.setEmail(email);
        return user;
    }

    @ParameterizedTest
    @ArgumentsSource(UserBadArgumentProvider.class)
    void isUserValid_ShouldReturnException(String name, String surname, String midname, String email) {
        ModelMapper mapper = new ModelMapper();
        User user = setUserFields(name, surname, midname, email);
        UserDTO userDTO = mapper.map(user, UserDTO.class);
        DataBinder dataBinder = new DataBinder(userDTO);
        dataBinder.addValidators(userDTOValidator);
        assertThrows(ValidationException.class, dataBinder::validate);

    }

    @ParameterizedTest
    @ArgumentsSource(UserGoodArgumentProvider.class)
    void isUserValid_ShouldNotReturnException(String name, String surname, String midname, String email) {
        ModelMapper mapper = new ModelMapper();
        User user = setUserFields(name, surname, midname, email);
        UserDTO userDTO = mapper.map(user, UserDTO.class);
        DataBinder dataBinder = new DataBinder(userDTO);
        dataBinder.addValidators(userDTOValidator);
        assertDoesNotThrow(() -> dataBinder.validate());

    }

    @Test
    void saveOrUpdateUser_shouldSaveOrUpdateNewUser() {
        // given
        UserDTO dto = new UserDTO(1L,
                "Ansar",
                "Anvari",
                "Sigi",
                "ansar@yandex.ru", "");

        User user = new User();
        user.setId(1L);
        user.setName("Ansar");
        user.setSurname("Anvari");
        user.setMidname("Sigi");
        user.setEmail("ansar@yandex.ru");


        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        ModelMapper mapper = new ModelMapper();
        User user1 = userService.findById(1L);
        UserDTO userDTO = mapper.map(user1, UserDTO.class);
        assertThat(userDTO).usingRecursiveComparison().isEqualTo(dto);
    }

}
