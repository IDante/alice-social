package com.example.alice.services.user;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class UserBadArgumentProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of("", "Anvari", "Sigi", "ansar@yandex.ru"),
                Arguments.of("Ansar", "", "Sigi", "ansar@yandex.ru"),
                Arguments.of("Ansar", "Anvari", "Sigi", "@yandex.ru"),
                Arguments.of("Ansar", "Anvari", "Sigi", "ansar@.ru"),
                Arguments.of("Ansar", "Anvari", "Sigi", "ansar@ru"),
                Arguments.of("Ansar", "Anvari", "Sigi", "")
        );
    }
}