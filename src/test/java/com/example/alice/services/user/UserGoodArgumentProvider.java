package com.example.alice.services.user;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class UserGoodArgumentProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of("Ansar", "Anvari", "Sigi", "ansar@yandex.ru"),
                Arguments.of("Ansar", "Anvari", "", "ansar@yandex.ru")
        );
    }
}