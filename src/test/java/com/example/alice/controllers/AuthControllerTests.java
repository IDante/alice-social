package com.example.alice.controllers;

import com.example.alice.models.users.User;
import com.example.alice.repos.UserRepository;
import com.example.alice.responses.AuthResponse;
import com.example.alice.services.auth.AuthService;
import jakarta.servlet.ServletContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class AuthControllerTests {
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @MockBean
    private AuthService authService;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }
    @Test
    public void givenWac_whenServletContext_thenItProvidesGreetController() {
        ServletContext servletContext = webApplicationContext.getServletContext();
        Assertions.assertNotNull(servletContext);
        Assertions.assertTrue(servletContext instanceof MockServletContext);
        Assertions.assertNotNull(webApplicationContext.getBean("authController"));
    }
    @Test
    public void registrationOkTest() throws Exception{
        String request = """
                {
                  "email": "user@example.com",
                  "password": "string"
                }
                """;
        when(authService.register(any())).thenReturn(AuthResponse.builder()
                .token("123123")
                .id(1L)
                .build());
        this.mockMvc.perform(post("/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void loginOkTest() throws Exception{
        String request = """
                {
                  "email": "user@example.com",
                  "password": "string"
                }
                """;
        when(authService.authenticateEmail(any())).thenReturn(AuthResponse.builder()
                .token("123123")
                .id(1L)
                .build());
        this.mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void loginYandexOkTest() throws Exception{
        String request = """
                {
                  "access_token": "apsiovnadionaovbaeivn"
                }
                """;
        when(authService.authYandex(any())).thenReturn(AuthResponse.builder()
                .token("123123")
                .id(1L)
                .build());
        this.mockMvc.perform(post("/auth/login/yandex")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk());

    }
}
